import argparse
import re
import json


def load_json(path):
    with open(path) as json_file:
        data = json.load(json_file)
    return data


def load_text_file(path):
    with open(path) as input_file:
        data = input_file.readlines()
    return list(data)


def merge_lists(args):
    """The function filters the lists by the number of stars and merges them."""
    gh_list = load_json(args.g)
    gh_len = len(gh_list)
    gh_list = [element for element in gh_list if element['stars'] >= args.s]

    if args.r != "":
        lib_list = load_json(args.r)
        lib_len = len(lib_list)
        lib_list = [
            element for element in lib_list if element['stargazers_count'] >= args.s]
        print("Removed {} repositories with less than {} stars - {} repositories remain".format(
            (gh_len + lib_len) - (len(gh_list) + len(lib_list)), args.s, (len(gh_list) + len(lib_list))))

        merged_list = [repo['full_name'] for repo in lib_list]
    else:
        merged_list = []
        print("Removed {} repositories with less than {} stars - {} repositories remain".format(
            (gh_len) - (len(gh_list)), args.s, (len(gh_list))))

    for repo in gh_list:
        merged_list.append("{}/{}".format(repo['author'], repo['repo']))
    merged_list = set(merged_list)
    print("{} repositories remain after merging".format(len(merged_list)))

    with open("merged_list_{0}.spec".format(re.findall(r"github_repos_(.*).json", args.g)[0]), 'w') as outfile:
        for repo in list(merged_list)[:int(args.l)]:
            outfile.write(repo + "\n")
    print("Saved results to merged_list_{0}.spec".format(
        re.findall(r"github_repos_(.*).json", args.g)[0]))


def create_dataset_list(args):
    """Uses the repository list with dependencies to a type checker and the repository list with dependencies
     to a framework, builds the intersection and saves it as a finished dataset list."""
    merged_list = load_text_file(args.g)
    merged_len = len(merged_list)

    mypy_list = load_text_file(args.m)

    dataset_list = set(merged_list).intersection(set(mypy_list))
    framework = re.findall(r"merged_list_(.*).spec", args.g)[0]
    type_checker = re.findall(r"merged_list_(.*).spec", args.m)[0]
    print("Found {}(/{}) repositories with dependencies to {}".format(len(dataset_list),
          merged_len, type_checker))

    with open("dataset_repo_list_{0}.spec".format(framework), 'w') as outfile:
        for repo in list(dataset_list)[:args.l]:
            outfile.write(
                "https://github.com/{}.git\n".format(repo.replace("\n", "")))
    print("Saved results to dataset_repo_list_{0}.spec".format(framework))


def main():
    """A script to merge the downloaded repository lists from GitHub and Libraries.io and create the final dataset repository list """

    arg_parser = argparse.ArgumentParser()
    sub_parsers = arg_parser.add_subparsers(dest='cmd')
    merge_parser = sub_parsers.add_parser('merge')

    merge_parser.add_argument('-g', required=True, type=str,
                              help="Path of the GitHub repository list")
    merge_parser.add_argument('-r', required=False, type=str, default="",
                              help="Path of the Libraries.io repository list")
    merge_parser.add_argument('-s', required=False, type=int, default=0,
                              help="Minimum number of stars of the repositories")
    merge_parser.add_argument('-l', required=False, type=int,
                              default=5000, help="Maximum number of repositories")
    merge_parser.set_defaults(func=merge_lists)

    insec_parser = sub_parsers.add_parser('intersection')

    insec_parser.add_argument('-g', required=True, type=str,
                              help="Path of the merged repository list")
    insec_parser.add_argument('-m', required=True, type=str,
                              help="Path of the mypy repository list")
    insec_parser.add_argument('-l', required=False, type=int,
                              default=5000, help="Maximum number of repositories")
    insec_parser.set_defaults(func=create_dataset_list)

    args = arg_parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
