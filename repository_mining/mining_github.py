import argparse
import requests
import json
import time
from bs4 import BeautifulSoup
import sys


def extract_stars(json):
    try:
        return int(json['stars'])
    except:
        return 0


def mining_github(url, limit):
    data = []
    urls = []
    while True:
        try:
            r = requests.get(url)
            soup = BeautifulSoup(r.content, "html.parser")

            for t in soup.findAll("div", {"class": "Box-row"}):
                author = t.find(
                    'a', {"data-repository-hovercards-enabled": ""}).text
                repo = t.find('a', {"data-hovercard-type": "repository"}).text
                stars = t.find(
                    'span', {"class": "color-fg-muted text-bold pl-3"}).text
                stars = stars.replace(',', '')
                stars = int(stars.replace('\n', ' '))
                data.append({'author': author, 'repo': repo, 'stars': stars})
            paginationContainer = soup.find(
                "div", {"class": "paginate-container"}).find_all('a')
            print("Found {} Repositories".format(len(data)))
            if len(paginationContainer) == 0:
                print(url)
                print(len(urls))
                break
            elif len(paginationContainer) > 1:
                paginationContainer = paginationContainer[1]
            else:
                paginationContainer = paginationContainer[0]

            if paginationContainer:
                url = paginationContainer["href"]
            else:
                break
            time.sleep(0.5)
            if url in urls:
                break
            else:
                urls.append(url)
            if len(data) >= limit:
                data.sort(key=extract_stars, reverse=True)
                return data
        except:
            print(f"URL: {url}")
            if len(data) != 0:
                with open('temp_github_results.json', 'w') as outfile:
                    json.dump(data, outfile)
                print("Saved temporary results to temp_github_results.json.")
            print("Unexpected error:", sys.exc_info())
    data.sort(key=extract_stars, reverse=True)
    return data


def main():
    """The script searches on GitHub for all dependent repositories for the specified framework and 
    stores the name and number of stars in a JSON file. A limit of repositories can be set. """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-a', required=True, type=str,
                            help="Author of the repository")
    arg_parser.add_argument('-r', required=True,
                            type=str, help="Repository name")
    arg_parser.add_argument('-l', required=False, type=int,
                            default=5000, help="Maximum number of repositories")
    args = arg_parser.parse_args()

    url = 'https://github.com/{0}/{1}/network/dependents'.format(
        args.a, args.r)
    data = mining_github(url, args.l)
    print("Search was successfully completed")

    with open("github_repos_{0}.json".format(args.r), 'w') as outfile:
        json.dump(data[:args.l], outfile)
    print("Saved results to github_repos_{0}.json".format(args.r))


if __name__ == "__main__":
    main()
