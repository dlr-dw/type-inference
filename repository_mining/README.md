# Scripts for Repository Mining

This folder contains the scripts to create your own dataset list. In the first two steps, we search for repositories depending on a specific library on the platforms GitHub and Libraries. Afterward, we merge these lists and create a dataset specification. 

## How to create a Dataset List

### Download Repository Lists

1. Find Repositories on Libraries.io with dependencies to a given framework
   

   ```shell
    python mining_libraries.py -a <Platform> -r <Framework> -k <API Key> -l <Limit>

    python mining_libraries.py -a pypi -r flask -k <API Key> -l 1000

   ```

2. Find Repositories on GitHub.com with dependencies to a given framework

   ```shell
   python mining_github.py -a <Username> -r <Framework> -l <Limit>

   python mining_github.py -a pallets -r flask -l 1000

   python mining_github.py -a python -r mypy -l 1000

   ```

### Create Dataset Lists

3. Merge repository lists

   ```shell
   python processing_repo_lists.py merge -g <GitHub List> -r <Libraries.io List> -s <Stars> -l <Limit>

   python processing_repo_lists.py merge -g github_repos_flask.json -r libraries_repos_flask.json

   python processing_repo_lists.py merge -g github_repos_mypy.json

   ```
4. Find intersection repositories and create a dataset specification

   ```shell
   python processing_repo_lists.py intersection -g <Merged Repository List> -m <Static Type Checker (e.g. Mypy) List> -l <Limit>

   python processing_repo_lists.py intersection -g merged_list_flask.spec -m merged_list_mypy.spec
   ```