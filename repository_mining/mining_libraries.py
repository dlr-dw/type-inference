import argparse
import requests
import json
import time
import sys


def extract_stars(json):
    try:
        return int(json['stargazers_count'])
    except:
        return 0


def mining_libraries(url, params, limit):
    page_count = 1
    data = {}
    while True:
        params.update(page=page_count)
        r = requests.get(url=url, params=params)
        if r is None:
            print("Waiting...")
            time.sleep(30)
            r = requests.get(url=url, params=params)
        if r.status_code != 200:
            print(
                f"Unexpected error: Server responded with status code {r.status_code}")
            break
        try:
            if len(r.json()) == 0:
                break
            if page_count == 1:
                data = r.json()
            else:
                data.extend(r.json())
        except:
            print("Error on page: {}".format(page_count))
            if len(data) != 0:
                with open('temp_libraries_results.json', 'w') as outfile:
                    json.dump(data, outfile)
                print("Saved temporary results to temp_libraries_results.json.")
            print("Unexpected error:", sys.exc_info()[0])
        page_count += 1
        time.sleep(0.8)
        print("Found {0} Repositories".format(len(data)))
        if len(data) >= limit:
            data.sort(key=extract_stars, reverse=True)
            return data
    sorted(data, key=extract_stars, reverse=True)
    #data.sort(key=extract_stars, reverse=True)
    return data


def main():
    """The script searches on Libraries.io for all dependent repositories for the specified framework and 
    stores the name and number of stars in a JSON file. A limit of repositories can be set. """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-a', required=True, type=str,
                            help="Platform (e.g. pypi)")
    arg_parser.add_argument('-r', required=True, type=str,
                            help="Name of the framework")
    arg_parser.add_argument('-k', required=True, type=str,
                            help="API key for libraries.io")
    arg_parser.add_argument('-l', required=False, type=int,
                            default=5000, help="Maximum number of repositories")
    args = arg_parser.parse_args()

    url = "https://libraries.io/api/{0}/{1}/dependent_repositories".format(
        args.a, args.r)

    params = {'api_key': args.k, 'per_page': 100, 'page': 1}

    data = mining_libraries(url, params, args.l)
    print("Search was successfully completed")

    with open("libraries_repos_{0}.json".format(args.r), 'w') as outfile:
        json.dump(data, outfile)
        #json.dump(data[:args.l], outfile)
    print("Saved results to libraries_repos_{0}.json".format(args.r))


if __name__ == "__main__":
    main()
