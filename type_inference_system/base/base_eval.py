import abc
import logging
import re
from collections import defaultdict

import numpy as np
import torch
import torch.nn as nn
from models.triplet_model import create_knn_index
from sklearn.metrics import classification_report, f1_score


class BaseEval(metaclass=abc.ABCMeta):
    def __init__(self, data, device, config, logger):
        self.data = data
        self.device = device
        self.config = config
        self.logger = logger
        self.result_log = []
        self.validation_log = []

    def eval_type_embed(
        self,
        y_pred,
        y_true,
        common_types,
        log_summary,
        eval_mode,
        top_n=1,
    ):

        all_common_types = 0
        corr_common_types = 0
        all_rare_types = 0
        corr_rare_types = 0
        all_insec_types = 0
        corr_insec_types = 0

        best_pred = []

        common_mask = np.array([False] * len(y_pred), dtype=np.bool)
        rare_mask = np.array([False] * len(y_pred), dtype=np.bool)

        for idx, p in enumerate(y_pred):

            if y_true[idx] in self.data.intersection_types:
                all_insec_types += 1
                if y_true[idx] in p[:top_n]:
                    corr_insec_types += 1

            if y_true[idx] in common_types:
                all_common_types += 1
                common_mask[idx] = True
                if y_true[idx] in p[:top_n]:
                    corr_common_types += 1
            else:
                all_rare_types += 1
                rare_mask[idx] = True
                if y_true[idx] in p[:top_n]:
                    corr_rare_types += 1
            if y_true[idx] in p[:top_n]:
                best_pred.append(y_true[idx])
            else:
                best_pred.append(p[0])

        acc_all = (corr_common_types + corr_rare_types) / len(y_pred) * 100.0
        acc_common = (corr_common_types / (all_common_types *
                      100.0)) if all_common_types > 0 else 0
        acc_rare = (corr_rare_types / (all_rare_types * 100.0)
                    ) if all_rare_types > 0 else 0
        acc_insec = (corr_insec_types / (all_insec_types * 100)
                     ) if all_insec_types > 0 else 0

        report = classification_report(
            y_true, best_pred, output_dict=True, zero_division=0)
        insec_report = classification_report(
            y_true, best_pred, output_dict=True, labels=self.data.intersection_types, zero_division=0
        )
        w_insec_f = f1_score(
            y_true,
            best_pred,
            average="weighted",
            labels=self.data.intersection_types,
            zero_division=0
        )
        micro_insec_f = f1_score(
            y_true, best_pred, average="micro", labels=np.unique(best_pred), zero_division=0
        )
        micro_f = f1_score(y_true, best_pred, average="micro", zero_division=0)
        macro_f = f1_score(y_true, best_pred, average="macro", zero_division=0)

        log_summary.update({
            "exact_match_all_" + str(eval_mode): float(acc_all),
            "exact_match_common_" + str(eval_mode): float(acc_common),
            "exact_match_rare_" + str(eval_mode): float(acc_rare),
            "exact_match_insec_" + str(eval_mode): float(acc_insec),
            "micro_insec_f1_" + str(eval_mode): float(micro_insec_f),
            "micro_f1_" + str(eval_mode): float(micro_f),
            "macro_f1_" + str(eval_mode): float(macro_f),
            "weighted_f1_" + str(eval_mode): float(report["weighted avg"]["f1-score"]),
            "weighted_recall_" + str(eval_mode): float(report["weighted avg"]["recall"]),
            "weighted_precision_" + str(eval_mode): float(report["weighted avg"]["precision"]),
            "weighted_insec_f1_" + str(eval_mode): float(w_insec_f),
            "weighted_insec_recall_" + str(eval_mode): float(insec_report["weighted avg"]["recall"]),
            "weighted_insec_precision_" + str(eval_mode): float(insec_report["weighted avg"]["precision"]),
        })

        return log_summary

    def eval_parametric_match(
        self,
        y_pred: np.array,
        y_true: np.array,
        common_types: set,
        log_summary,
        eval_mode,
        top_n: int = 1,
    ):

        all_param_common_types = 0
        corr_param_common_types = 0
        all_param_rare_types = 0
        corr_param_rare_types = 0
        param_type_match = r"(.+)\[(.+)\]"

        def pred_param_types(pred_types: np.array, true_param_type):
            no_match = 0
            for p in pred_types:
                if re.match(param_type_match, p):
                    if true_param_type.group(1) == re.match(param_type_match, p).group(
                        1
                    ):
                        no_match += 1
                        break

            return no_match

        for idx, t in enumerate(y_true):
            matched_param_type = re.match(param_type_match, t)
            if t in common_types:
                all_param_common_types += 1
                if t in y_pred[idx][:top_n]:
                    corr_param_common_types += 1
                elif matched_param_type:
                    corr_param_common_types += pred_param_types(
                        y_pred[idx], matched_param_type
                    )

            else:
                all_param_rare_types += 1
                if t in y_pred[idx][:top_n]:
                    corr_param_rare_types += 1
                elif matched_param_type:
                    corr_param_rare_types += pred_param_types(
                        y_pred[idx], matched_param_type
                    )
        acc_all_param = (corr_param_common_types +
                         corr_param_rare_types) / len(y_pred) * 100.0
        acc_common_param = corr_param_common_types / all_param_common_types * 100.0
        acc_rare_param = corr_param_rare_types / all_param_rare_types * 100.0

        log_summary.update({
            "parametric_match_all_" + str(eval_mode): float(acc_all_param),
            "parametric_match_common_" + str(eval_mode): float(acc_common_param),
            "parametric_match_rare_" + str(eval_mode): float(acc_rare_param)
        })

        return log_summary

    def calculate_mrr_score(
        self, y_pred, y_true, common_types, log_summary, eval_mode, top_n=1
    ):
        param_type_match = r"(.+)\[(.+)\]"

        def pred_types_fix(y_true, y_pred):
            for i, p in enumerate(y_pred[:top_n]):
                if p == y_true:
                    return p, 1 / (i + 1)

            return y_pred[0], 0.0

        def is_param_correct(true_param_type: str, pred_types: np.array):
            no_match = 0
            r = 0.0
            for i, p in enumerate(pred_types):
                if re.match(param_type_match, p):
                    if re.match(param_type_match, true_param_type).group(1) == re.match(
                        param_type_match, p
                    ).group(1):
                        no_match += 1
                        r = 1 / (i + 1)
                        break
                else:
                    if (
                        re.match(param_type_match,
                                 true_param_type).group(1).lower()
                        == p.lower()
                    ):
                        no_match += 1
                        r = 1 / (i + 1)
                        break

            return no_match, r

        all_common_types = 0
        corr_common_types = 0
        all_rare_types = 0
        corr_rare_types = 0

        all_param_common_types = 0
        corr_param_common_types = 0
        all_param_rare_types = 0
        corr_param_rare_types = 0

        mrr = []
        mrr_exact_ubiq = []
        mrr_exact_comm = []
        mrr_exact_rare = []

        mrr_param_ubiq = []
        mrr_param_comm = []
        mrr_param_rare = []

        for p, gt in zip(y_pred, y_true):
            top_n_pred, r = pred_types_fix(gt, p)
            mrr.append(r)

            if gt in common_types:
                all_common_types += 1
                mrr_exact_comm.append(r)
                if gt == top_n_pred:
                    corr_common_types += 1
                elif bool(re.match(r"(.+)\[(.+)\]", gt)):
                    m, pr = is_param_correct(gt, [i for i in p[:top_n]])
                    mrr_param_comm.append(pr)
                    corr_param_common_types += m
            else:
                all_rare_types += 1
                mrr_exact_rare.append(r)
                if gt == top_n_pred:
                    corr_rare_types += 1
                elif bool(re.match(r"(.+)\[(.+)\]", gt)):
                    m, pr = is_param_correct(gt, [i for i in p[:top_n]])
                    mrr_param_rare.append(pr)
                    corr_param_rare_types += m

        log_summary.update({
            "mrr_exact_all_" + str(eval_mode): float(np.mean(mrr) * 100),
            "mrr_exact_common_" + str(eval_mode): float(np.mean(mrr_exact_comm) * 100),
            "mrr_exact_rare_" + str(eval_mode): float(np.mean(mrr_exact_rare) * 100),
            "mrr_parametric_all_" + str(eval_mode): float(np.mean(mrr_exact_comm + mrr_exact_rare + mrr_param_comm + mrr_param_rare) * 100),
            "mrr_parametric_common_" + str(eval_mode): float(np.mean(mrr_param_comm + mrr_exact_comm) * 100),
            "mrr_parametric_rare_" + str(eval_mode): float(np.mean(mrr_param_rare + mrr_exact_rare) * 100)
        })

        return log_summary

    def compute_types_score(
        self, types_dist: list, types_idx: list, types_embed_labels: np.array
    ):
        types_dist = 1 / (np.array(types_dist) + 1e-10) ** 2
        types_dist /= np.sum(types_dist)
        types_score = defaultdict(int)
        for n, d in zip(types_idx, types_dist):
            types_score[types_embed_labels[n]] += d

        return sorted(
            {t: s for t, s in types_score.items()}.items(),
            key=lambda kv: kv[1],
            reverse=True,
        )

    def predict_type_embed(
        self,
        types_embed_array: np.array,
        types_embed_labels: np.array,
        indexed_knn,
        k: int,
    ) -> np.array:
        """
        Predict type of given type embedding vectors
        """

        pred_types_embed = []
        pred_types_score = []
        pred_types_distance = []
        for embed_vec in types_embed_array:
            idx, dist = indexed_knn.get_nns_by_vector(
                embed_vec, k, include_distances=True
            )
            pred_idx_scores = self.compute_types_score(
                dist, idx, types_embed_labels)
            pred_types_embed.append([i for i, s in pred_idx_scores])
            pred_types_score.append(pred_idx_scores[0])
            pred_types_distance.append(dist[0])

        return np.array(pred_types_embed, dtype=object), pred_types_score, pred_types_distance

    @abc.abstractmethod
    def evaluate_test_data(self):
        raise NotImplementedError

    def do_validation(self, model, criterion):
        raise NotImplementedError(
            "The validation method must be revised"
        )
        logging.info("Start valdiation...")
        valid_target_loss = 0
        valid_source_loss = 0

        with torch.no_grad():
            model.eval()

            if isinstance(model, nn.DataParallel):
                main_feature_extractor = model.module.feature_extractor
            else:
                main_feature_extractor = model.feature_extractor

            computed_embed_batches_train = []
            computed_embed_labels_train = []
            computed_embed_batches_valid = []
            computed_embed_labels_valid = []

            computed_embed_batches_valid_target = []
            computed_embed_labels_valid_target = []

            for batch_i, (a, p, n) in enumerate(self.data.train_loader):
                output_a = main_feature_extractor(
                    *(s.to(self.device) for s in a[0]))
                computed_embed_batches_train.append(
                    output_a.data.cpu().numpy())
                computed_embed_labels_train.append(a[1].data.cpu().numpy())

            for batch_i, (anchor, positive_ex, negative_ex) in enumerate(
                self.data.valid_loader
            ):
                positive_ex, _ = positive_ex[0], positive_ex[1]
                negative_ex, _ = negative_ex[0], negative_ex[1]

                if (self.config.trainings_mode == "dann"
                        ):
                    anchor_embed, positive_ex_embed, negative_ex_embed, _, _ = model(
                        anchor[0], positive_ex, negative_ex
                    )
                else:
                    anchor_embed, positive_ex_embed, negative_ex_embed = model(
                        anchor[0], positive_ex, negative_ex
                    )

                loss = criterion(
                    anchor_embed, positive_ex_embed, negative_ex_embed)
                # add domain loss
                valid_source_loss += loss.item()

                output_a = main_feature_extractor(
                    *(s.to(self.device) for s in anchor[0])
                )
                computed_embed_batches_valid.append(
                    output_a.data.cpu().numpy())
                computed_embed_labels_valid.append(
                    anchor[1].data.cpu().numpy())

            annoy_index = create_knn_index(
                np.vstack(computed_embed_batches_train),
                None,
                computed_embed_batches_train[0].shape[1],
                20,
            )
            pred_valid_embed, _, _ = self.predict_type_embed(
                np.vstack(computed_embed_batches_valid),
                np.hstack(computed_embed_labels_train),
                annoy_index,
                10,
            )
            (
                acc_all,
                acc_common,
                acc_rare,
                acc_insec,
            ) = self.eval_type_embed(
                pred_valid_embed,
                np.hstack(computed_embed_labels_valid),
                self.data.common_types,
            )
            logging.info(
                "Source: E-All: %.2f | E-Comm: %.2f | E-rare: %.2f"
                % (acc_all, acc_common, acc_rare)
            )

            for batch_i, (anchor_t, positive_ex_t, negative_ex_t) in enumerate(
                self.data.valid_loader_target
            ):
                positive_ex_t, _ = positive_ex_t[0], positive_ex_t[1]
                negative_ex_t, _ = negative_ex_t[0], negative_ex_t[1]

                if (
                    self.config.trainings_mode == "source-only-ext"
                    or self.config.trainings_mode == "dann"
                ):
                    (
                        anchor_embed_t,
                        positive_ex_embed_t,
                        negative_ex_embed_t,
                        _,
                        _,
                    ) = model(anchor_t[0], positive_ex_t, negative_ex_t)
                else:
                    anchor_embed_t, positive_ex_embed_t, negative_ex_embed_t = model(
                        anchor_t[0], positive_ex_t, negative_ex_t
                    )
                loss_t = criterion(
                    anchor_embed_t, positive_ex_embed_t, negative_ex_embed_t
                )
                valid_target_loss += loss_t.item()

                output_a = main_feature_extractor(
                    *(s.to(self.device) for s in anchor_t[0])
                )
                computed_embed_batches_valid_target.append(
                    output_a.data.cpu().numpy())
                computed_embed_labels_valid_target.append(
                    anchor_t[1].data.cpu().numpy()
                )

            pred_valid_embed_target, _, _ = self.predict_type_embed(
                np.vstack(computed_embed_batches_valid_target),
                np.hstack(computed_embed_labels_train),
                annoy_index,
                10,
            )
            (
                acc_all_target,
                acc_common_target,
                acc_rare_target,
                acc_insec_t,
            ) = self.eval_type_embed(
                pred_valid_embed_target,
                np.hstack(computed_embed_labels_valid_target),
                self.data.common_types,
            )
            logging.info(
                "Target: E-All: %.2f | E-Comm: %.2f | E-rare: %.2f"
                % (acc_all_target, acc_common_target, acc_rare_target)
            )

        return (
            float(valid_source_loss),
            float(valid_target_loss),
            float(acc_all),
            float(acc_all_target),
            float(acc_common),
            float(acc_common_target),
            float(acc_rare),
            float(acc_rare_target),
            float(acc_insec),
            float(acc_insec_t),
        )
