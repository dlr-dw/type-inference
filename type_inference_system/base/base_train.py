import abc
import logging
import os
from typing import Tuple

import numpy as np
import torch


class BaseTrain(metaclass=abc.ABCMeta):
    def __init__(self, data, config, logger):
        self.data = data
        self.config = config
        self.logger = logger

    def train(self):
        for epoch in range(0, self.config.num_epochs + 1, 1):
            self.train_epoch()

    @abc.abstractmethod
    def train_epoch(self):
        raise NotImplementedError

    @abc.abstractmethod
    def train_step(self):
        raise NotImplementedError

    def compute_type_embedding(
        self, model, data_loader, device
    ) -> Tuple[np.array, np.array]:

        computed_embed_batches = []
        computed_embed_labels = []

        for batch_i, (a, p, n) in enumerate(data_loader):
            model.eval()
            with torch.no_grad():
                output_a = model(*(s.to(device) for s in a[0]))
                computed_embed_batches.append(output_a.data.cpu().numpy())
                computed_embed_labels.append(a[1].data.cpu().numpy())

        return np.vstack(computed_embed_batches), np.hstack(computed_embed_labels)

    def get_embeddings(self, model, device):
        if isinstance(model, torch.nn.DataParallel):
            main_feature_extractor = model.module.feature_extractor
        else:
            main_feature_extractor = model.feature_extractor

        train_type_embed, embed_train_labels = self.compute_type_embedding(
            main_feature_extractor, self.data.train_loader, device
        )
        valid_type_embed, embed_valid_labels = self.compute_type_embedding(
            main_feature_extractor, self.data.valid_loader, device
        )
        (
            valid_type_embed_target,
            embed_valid_labels_target,
        ) = self.compute_type_embedding(
            main_feature_extractor, self.data.valid_loader_target, device
        )

        (
            train_type_embed_target,
            embed_train_labels_target,
        ) = self.compute_type_embedding(
            main_feature_extractor, self.data.train_loader_target, device
        )
        (
            test_type_embed_target,
            embed_test_labels_target,
        ) = self.compute_type_embedding(
            main_feature_extractor, self.data.test_loader_target, device
        )
        test_type_embed, embed_test_labels = self.compute_type_embedding(
            main_feature_extractor, self.data.test_loader, device
        )

        if self.config.save_features:
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "train_type_embed.npy"),
                train_type_embed,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_train_labels.npy"),
                embed_train_labels,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "valid_type_embed.npy"),
                valid_type_embed,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_valid_labels.npy"),
                embed_valid_labels,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "test_type_embed.npy"),
                test_type_embed,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_test_labels.npy"),
                embed_test_labels,
            )

            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "train_type_embed_t.npy"),
                train_type_embed_target,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_train_labels_t.npy"),
                embed_train_labels_target,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "valid_type_embed_t.npy"),
                valid_type_embed_target,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_valid_labels_t.npy"),
                embed_valid_labels_target,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "test_type_embed_t.npy"),
                test_type_embed_target,
            )
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             "embed_test_labels_t.npy"),
                embed_test_labels_target,
            )
            logging.info("Saved all features")
        return (
            [train_type_embed, valid_type_embed, test_type_embed],
            [embed_train_labels, embed_valid_labels, embed_test_labels],
            [
                train_type_embed_target,
                valid_type_embed_target,
                test_type_embed_target,
            ],
            [
                embed_train_labels_target,
                embed_valid_labels_target,
                embed_test_labels_target,
            ],
        )
