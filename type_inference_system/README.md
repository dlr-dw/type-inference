# Type Inference System

This folder contains the complete experiment pipeline, which can be used for cross-domain and novelty detection experiments.

## How to use the pipeline

### Start Pipeline
The first step is to create a valid configuration file. An example configuration with explanations can be found in the config folder.

```shell
python main.py -c configs/<Config File>
```
During the execution of the pipeline, an experiment folder is created in the root_path. This folder contains for each executed experiment a folder with the experiment name. It includes a checkpoint folder in which features and model weights are saved. Furthermore, there is a summary folder, which stores log and result files, as well as the configuration file.
If the pipeline is executed several times with one configuration, the same experiment folder is used again and the results are added to the previous ones.

### Perform Evaluation
The notebooks show_results and novelty_detection_research can be used to evaluate the experiments. The latter is used for the evaluation of novelty detection experiments. For the evaluation of cross-domain experiments, we provide the show_results notebook. It automatically considers multiple executions of an experiment and offers several options for the evaluation of experiments.

## ToDo

* [ ] The validation method must be revised (therefore validation must be set to false in the configuration file)
