from typing import Tuple
from torch.utils.data import TensorDataset
import torch


class TripletDataset(torch.utils.data.Dataset):

    def __init__(self, *in_sequences: torch.Tensor, labels: torch.Tensor, dataset_name: str,
                 train_mode: bool = True, source_domain: bool = True):
        self.data = TensorDataset(*in_sequences)
        self.labels = labels
        self.dataset_name = dataset_name
        self.train_mode = train_mode
        self.source_domain = source_domain

        self.get_item_func = self.get_item_train if self.train_mode else self.get_item_test

    def get_item_train(self, index: int) -> Tuple[Tuple[torch.Tensor, torch.Tensor],
                                                  Tuple[torch.Tensor, torch.Tensor], Tuple[torch.Tensor, torch.Tensor]]:
        """
        It returns three tuples. Each one is a (data, label)
         - The first tuple is (data, label) at the given index
         - The second tuple is similar (data, label) to the given index
         - The third tuple is different (data, label) from the given index 
        """

        # Find a similar datapoint randomly
        mask = self.labels == self.labels[index]
        # Making sure that the similar pair is NOT the same as the given index
        mask[index] = False
        mask = mask.nonzero()
        a = mask[torch.randint(high=len(mask), size=(1,))][0]

        # Find a different datapoint randomly
        mask = self.labels != self.labels[index]
        mask = mask.nonzero()
        b = mask[torch.randint(high=len(mask), size=(1,))][0]

        #domain_label = 0 if self.source_domain else 1

        return (self.data[index], self.labels[index]), (self.data[a.item()], self.labels[a.item()]), \
               (self.data[b.item()], self.labels[b.item()])

    def get_item_test(self, index: int) -> Tuple[Tuple[torch.Tensor, torch.Tensor], list, list]:
        return (self.data[index], self.labels[index]), [], []

    def __getitem__(self, index: int) -> Tuple[Tuple[torch.Tensor, torch.Tensor],
                                               Tuple[torch.Tensor, torch.Tensor], Tuple[torch.Tensor, torch.Tensor]]:
        return self.get_item_func(index)

    def __len__(self) -> int:
        return len(self.data)

    def get_labels(self):
        return self.labels
