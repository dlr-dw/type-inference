import logging
from collections import Counter
from os.path import exists, join
from time import time

import numpy as np
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader

from data_loader.triplet_dataset import TripletDataset


class DataGenerator:
    def __init__(self, config):
        self.config = config
        logging.info("Load and prepare datasets....")
        self.init_data_load_func()
        self.embed_source, self.label_source, self.embed_target, self.label_target = None, None, None, None
        load_data_t = time()
        self.load_and_prepare_data()
        logging.info("Loaded datasets in %.2f min" %
                     ((time() - load_data_t) / 60))

    def init_data_load_func(self):
        self.data_loading_comb = {
            "train": self.load_combined_train_data,
            "valid": self.load_combined_valid_data,
            "test": self.load_combined_test_data,
            "labels": self.load_combined_labels,
            "name": "combined",
        }

    def set_embedded_data(self, embed_source, label_source, embed_target, label_target):
        self.embed_source, self.label_source, self.embed_target, self.label_target = embed_source, label_source, embed_target, label_target

    def get_embedded_data(self):
        if not self.embed_source:
            train_type_embed = np.load(join(
                self.config.path_eval_data, "train_type_embed.npy"), allow_pickle=True)
            train_labels = np.load(join(
                self.config.path_eval_data, "embed_train_labels.npy"), allow_pickle=True)
            valid_type_embed = np.load(join(
                self.config.path_eval_data, "valid_type_embed.npy"), allow_pickle=True)
            valid_labels = np.load(join(
                self.config.path_eval_data, "embed_valid_labels.npy"), allow_pickle=True)
            test_type_embed = np.load(join(
                self.config.path_eval_data, "test_type_embed.npy"), allow_pickle=True)
            test_labels = np.load(join(
                self.config.path_eval_data, "embed_test_labels.npy"), allow_pickle=True)

            train_type_embed_target = np.load(join(
                self.config.path_eval_data, "train_type_embed_t.npy"), allow_pickle=True)
            train_labels_target = np.load(join(
                self.config.path_eval_data, "embed_train_labels_t.npy"), allow_pickle=True)
            valid_type_embed_target = np.load(join(
                self.config.path_eval_data, "valid_type_embed_t.npy"), allow_pickle=True)
            valid_labels_target = np.load(join(
                self.config.path_eval_data, "embed_valid_labels_t.npy"), allow_pickle=True)
            test_type_embed_target = np.load(join(
                self.config.path_eval_data, "test_type_embed_t.npy"), allow_pickle=True)
            test_labels_target = np.load(join(
                self.config.path_eval_data, "embed_test_labels_t.npy"), allow_pickle=True)

            self.embed_source = [train_type_embed,
                                 valid_type_embed, test_type_embed]
            self.label_source = [train_labels, valid_labels, test_labels]
            self.embed_target = [train_type_embed_target,
                                 valid_type_embed_target, test_type_embed_target, ]
            self.label_target = [train_labels_target,
                                 valid_labels_target, test_labels_target, ]

        return self.embed_source, self.label_source, self.embed_target, self.label_target

    def load_and_prepare_data(self):
        self.data_loading_funcs = self.data_loading_comb

        self.ds = TripletDataset

        X_id_train, X_tok_train, X_type_train = self.data_loading_funcs["train"](
            self.config.path_source_dataset
        )
        X_id_valid, X_tok_valid, X_type_valid = self.data_loading_funcs["valid"](
            self.config.path_source_dataset
        )
        X_id_test, X_tok_test, X_type_test = self.data_loading_funcs["test"](
            self.config.path_source_dataset
        )
        Y_all_train, Y_all_valid, Y_all_test = self.data_loading_funcs["labels"](
            self.config.path_source_dataset
        )

        (
            X_id_train_target,
            X_tok_train_target,
            X_type_train_target,
        ) = self.data_loading_funcs["train"](self.config.path_target_dataset)
        (
            X_id_valid_target,
            X_tok_valid_target,
            X_type_valid_target,
        ) = self.data_loading_funcs["valid"](self.config.path_target_dataset)
        (
            X_id_test_target,
            X_tok_test_target,
            X_type_test_target,
        ) = self.data_loading_funcs["test"](self.config.path_target_dataset)
        (
            Y_all_train_target,
            Y_all_valid_target,
            Y_all_test_target,
        ) = self.data_loading_funcs["labels"](self.config.path_target_dataset)

        self.intersection_types = np.intersect1d(
            np.unique(Y_all_train), np.unique(Y_all_test_target)
        )

        train_mask = self.select_data(Y_all_train, 3, target=True)

        X_id_train, X_tok_train, X_type_train, Y_all_train = (
            X_id_train[train_mask],
            X_tok_train[train_mask],
            X_type_train[train_mask],
            Y_all_train[train_mask],
        )

        valid_mask = self.select_data(Y_all_valid, 3, target=True)
        X_id_valid, X_tok_valid, X_type_valid, Y_all_valid = (
            X_id_valid[valid_mask],
            X_tok_valid[valid_mask],
            X_type_valid[valid_mask],
            Y_all_valid[valid_mask],
        )

        self.intersection_types = np.intersect1d(
            np.unique(Y_all_train), np.unique(Y_all_test_target)
        )

        train_mask_target = self.select_data(
            Y_all_train_target, 3, target=True)
        (
            X_id_train_target,
            X_tok_train_target,
            X_type_train_target,
            Y_all_train_target,
        ) = (
            X_id_train_target[train_mask_target],
            X_tok_train_target[train_mask_target],
            X_type_train_target[train_mask_target],
            Y_all_train_target[train_mask_target],
        )

        valid_mask_target = self.select_data(
            Y_all_valid_target, 3, target=True)
        (
            X_id_valid_target,
            X_tok_valid_target,
            X_type_valid_target,
            Y_all_valid_target,
        ) = (
            X_id_valid_target[valid_mask_target],
            X_tok_valid_target[valid_mask_target],
            X_type_valid_target[valid_mask_target],
            Y_all_valid_target[valid_mask_target],
        )

        count_types = Counter(Y_all_train.data.numpy())
        self.common_types = [
            t.item() for t in Y_all_train if count_types[t.item()] >= 100
        ]
        self.common_types = set(self.common_types)

        count_types_target = Counter(Y_all_train_target.data.numpy())
        self.common_types_target = [
            t.item() for t in Y_all_train_target if count_types[t.item()] >= 100
        ]
        self.common_types_target = set(self.common_types_target)

        logging.info(
            "Source dataset: training: {} val: {} test: {}".format(
                len(X_id_train), len(X_id_valid), len(X_tok_test)
            )
        )
        logging.info(
            "Source dataset: total types: {} common types(100): {}".format(
                len(count_types), len(self.common_types)
            )
        )
        logging.info(
            "Target dataset: training: {} val: {} test: {}".format(
                len(X_id_train_target), len(
                    X_id_valid_target), len(X_tok_test_target)
            )
        )
        logging.info(
            "Target dataset: total types: {} common types(100): {}".format(
                len(count_types_target), len(self.common_types_target)
            )
        )

        train_ds = self.ds(
            X_id_train,
            X_tok_train,
            X_type_train,
            labels=Y_all_train,
            dataset_name=self.data_loading_funcs["name"],
            train_mode=True,
        )

        num_worker = 8

        self.train_loader = DataLoader(
            train_ds,
            batch_size=self.config.batch_size,
            pin_memory=True,
            num_workers=num_worker,
            drop_last=True,
            shuffle=True,
        )
        self.valid_loader = DataLoader(
            self.ds(
                X_id_valid,
                X_tok_valid,
                X_type_valid,
                labels=Y_all_valid,
                dataset_name=self.data_loading_funcs["name"],
                train_mode=True,
            ),
            batch_size=self.config.batch_size,
            num_workers=num_worker,
        )
        self.test_loader = DataLoader(
            self.ds(
                X_id_test,
                X_tok_test,
                X_type_test,
                labels=Y_all_test,
                dataset_name=self.data_loading_funcs["name"],
                train_mode=False,
            ),
            batch_size=self.config.batch_size,
            num_workers=num_worker,
        )

        self.train_loader_target = DataLoader(
            self.ds(
                X_id_train_target,
                X_tok_train_target,
                X_type_train_target,
                labels=Y_all_train_target,
                dataset_name=self.data_loading_funcs["name"] + "_target",
                train_mode=True,
                source_domain=False,
            ),
            batch_size=self.config.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=num_worker,
            drop_last=True,
        )
        self.valid_loader_target = DataLoader(
            self.ds(
                X_id_valid_target,
                X_tok_valid_target,
                X_type_valid_target,
                labels=Y_all_valid_target,
                dataset_name=self.data_loading_funcs["name"] + "target",
                train_mode=True,
                source_domain=False,
            ),
            batch_size=self.config.batch_size,
            num_workers=num_worker,
        )
        self.test_loader_target = DataLoader(
            self.ds(
                X_id_test_target,
                X_tok_test_target,
                X_type_test_target,
                labels=Y_all_test_target,
                dataset_name=self.data_loading_funcs["name"] + "_target",
                train_mode=False,
            ),
            batch_size=self.config.batch_size,
            num_workers=num_worker,
        )

        self.domain_label_source = torch.from_numpy(
            np.ones(self.config.batch_size))
        self.domain_label_target = torch.from_numpy(
            np.zeros(self.config.batch_size))

        self.intersection_types = np.intersect1d(
            np.unique(Y_all_train), np.unique(Y_all_test_target)
        )
        logging.info(
            "Datasets: intersection types: {} ".format(
                len(self.intersection_types))
        )

    def to_one_hot(self, y, n_dims=None):
        """Take integer y (tensor or variable) with n dims and convert it to 1-hot representation with n+1 dims."""
        y_tensor = y.data if isinstance(y, Variable) else y
        y_tensor = y_tensor.type(torch.LongTensor).view(-1, 1)
        n_dims = n_dims if n_dims is not None else int(torch.max(y_tensor)) + 1
        y_one_hot = torch.zeros(
            y_tensor.size()[0], n_dims).scatter_(1, y_tensor, 1)
        y_one_hot = y_one_hot.view(*y.shape, -1)
        return Variable(y_one_hot) if isinstance(y, Variable) else y_one_hot

    def load_data_tensors_TW(self, filename, limit=-1):
        return torch.from_numpy(np.load(filename)).float()

    def load_flat_labels_tensors(self, filename):
        return torch.from_numpy(np.load(filename)).long()

    # Combined data
    def load_combined_train_data(self, root_path: str):
        at = f"_{self.config.available_types}_VTHs"

        if exists(
            join(
                root_path,
                "vectors",
                "train",
                "tokens_ret_train_datapoints_x.npy",
            )
        ):
            return (
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "identifiers_param_train_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "identifiers_ret_train_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "identifiers_var_train_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "tokens_param_train_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "tokens_ret_train_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "tokens_var_train_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "params_train_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "ret_train_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "var_train_aval_types_dp{}.npy".format(at),
                            )
                        ),
                    )
                ),
            )
        else:
            raise ValueError("No training data found!")

    def load_combined_valid_data(self, root_path: str):
        at = f"_{self.config.available_types}_VTHs"

        if exists(
            join(
                root_path,
                "vectors",
                "valid",
                "identifiers_ret_valid_datapoints_x.npy",
            )
        ):
            return (
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "identifiers_param_valid_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "identifiers_ret_valid_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "identifiers_var_valid_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "tokens_param_valid_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "tokens_ret_valid_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "tokens_var_valid_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "params_valid_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "ret_valid_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "var_valid_aval_types_dp{}.npy".format(at),
                            )
                        ),
                    )
                ),
            )
        else:
            raise ValueError("No validation data found!")

    def load_combined_test_data(self, root_path: str):
        at = f"_{self.config.available_types}_VTHs"

        if exists(
            join(
                root_path,
                "vectors",
                "test",
                "identifiers_ret_test_datapoints_x.npy",
            )
        ):
            return (
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "identifiers_param_test_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "identifiers_ret_test_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "identifiers_var_test_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "tokens_param_test_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "tokens_ret_test_datapoints_x.npy",
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "tokens_var_test_datapoints_x.npy",
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "params_test_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "ret_test_aval_types_dp{}.npy".format(at),
                            )
                        ),
                        self.load_data_tensors_TW(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "var_test_aval_types_dp{}.npy".format(at),
                            )
                        ),
                    )
                ),
            )
        else:
            raise ValueError("No test data found!")

    def load_combined_labels(self, root_path: str):
        if exists(join(root_path, "vectors", "train", "ret_train_dps_y_all.npy")):
            return (
                torch.cat(
                    (
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "params_train_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "train",
                                "ret_train_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(
                                root_path, "vectors", "train", "var_train_dps_y_all.npy"
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "params_valid_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "valid",
                                "ret_valid_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(
                                root_path, "vectors", "valid", "var_valid_dps_y_all.npy"
                            )
                        ),
                    )
                ),
                torch.cat(
                    (
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "params_test_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(
                                root_path,
                                "vectors",
                                "test",
                                "ret_test_dps_y_all.npy",
                            )
                        ),
                        self.load_flat_labels_tensors(
                            join(root_path, "vectors", "test",
                                 "var_test_dps_y_all.npy")
                        ),
                    )
                ),
            )
        else:
            raise ValueError("No labels found!")

    def select_data(self, data, n, target=False):
        """
        Selects data points that are frequent more than n times
        """

        mask = torch.tensor([False] * data.shape[0], dtype=torch.bool)
        counter = Counter(data.data.numpy())

        if target and self.config.train_insec_types:
            for i, d in enumerate(data):
                if counter[d.item()] >= n and d.item() in self.intersection_types:
                    mask[i] = True
        else:
            for i, d in enumerate(data):
                if counter[d.item()] >= n:
                    mask[i] = True

        return mask

    def get_unknown_data(self, data, train_labels):
        mask = torch.tensor([False] * data.shape[0], dtype=torch.bool)
        for i, d in enumerate(data):
            if d.item() not in train_labels:
                mask[i] = True
        return mask
