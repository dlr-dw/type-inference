import json
import os

import numpy as np
from bunch import Bunch


def get_config_from_json(json_file):
    with open(json_file, 'r') as config_file:
        config_dict = json.load(config_file)

    config = Bunch(config_dict)

    return config, config_dict

# ToDo: Implement a config validator


def check_config(config):
    pass


def process_config(args):
    json_file = args.config
    config, _ = get_config_from_json(json_file)
    if args.learning_rate != 'None':
        config.learning_rate = np.float32(args.learning_rate)
    if args.lam != 'None':
        config.lambda_coef = np.float32(args.lam)
    if args.reg != 'None':
        config.weight_decay = np.float32(args.reg)
    if args.typehints != 'None':
        config.available_types = str(args.typehints)
    config.summary_dir = os.path.join(config.root_path, "experiments", config.exp_name + "_lr_"+str(np.format_float_positional(np.float32(config.learning_rate), trim='-'))+"_lam_"+str(
        np.format_float_positional(np.float32(config.lambda_coef), trim='-'))+"_reg_"+str(np.format_float_positional(np.float32(config.weight_decay), trim='-')) + "_avaitype_"+str(config.available_types), "summary/")
    config.checkpoint_dir = os.path.join(config.root_path, "experiments", config.exp_name + "_lr_"+str(np.format_float_positional(np.float32(config.learning_rate), trim='-'))+"_lam_"+str(
        np.format_float_positional(np.float32(config.lambda_coef), trim='-'))+"_reg_"+str(np.format_float_positional(np.float32(config.weight_decay), trim='-')) + "_avaitype_"+str(config.available_types), "checkpoint/")
    return config
