import logging
from os.path import join, exists



class Logger:
    def __init__(self, config):
        self.config = config
        self.local_log = []
        self.test_results = []
        logging.basicConfig(
            filename=join(config.summary_dir, f"general_{config.run}.log"),
            level=logging.DEBUG,
            format="%(asctime)s %(levelname)s: %(message)s",
        )
        logging.info("Logger initialized")

    def reset(self):
        self.local_log = []
        self.test_results = []

    def write_log(self, path=None):
        header = "epoch"
        if path:
            log_path = path
        else:
            log_path = self.config.summary_dir
        result_log_ex = exists(join(log_path, "results.log"))
        if self.local_log:
            with open(join(log_path, f"parameter_{self.config.run}.log"), "a+") as file:
                for key in self.local_log[0]:
                    header = "{0},{1}".format(header, key)
                file.write(header + "\n")
                epoch = 0
                for parameters in self.local_log:
                    line = str(epoch)
                    for key in parameters:
                        line = "{0},{1}".format(line, parameters[key])
                    line = line + "\n"
                    file.write(line)
                    epoch += 1
        with open(join(log_path, "results.log"), "a+") as file:
            header = ""
            count = 0
            if not result_log_ex:
                for key in self.test_results[0]:
                    if count == 0:
                        header = str(key)
                        count += 1
                    else:
                        header = "{0},{1}".format(header, key)
                file.write(header + "\n")
            for t_results in self.test_results:
                count = 0
                for key in t_results:
                    if count == 0:
                        line = str(t_results[key])
                        count += 1
                    else:
                        line = "{0},{1}".format(line, t_results[key])
                line = line + "\n"
                file.write(line)
        self.reset()

    def log(self, parameters, test_results=False):
        if test_results:
            self.test_results = parameters
        else:
            self.local_log.append(parameters)
