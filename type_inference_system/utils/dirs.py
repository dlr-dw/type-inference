import os
import shutil
import glob


def create_dirs(dirs):
    try:
        for dir_ in dirs:
            if not os.path.exists(dir_):
                os.makedirs(dir_)
        return 0
    except Exception as err:
        print("Creating directories error: {0}".format(err))
        exit(-1)


def remove_dirs(dirs):
    try:
        for dir_ in dirs:
            if os.path.exists(dir_):
                shutil.rmtree(dir_)
        return 0
    except Exception as err:
        print("Removing directories error: {0}".format(err))
        exit(-1)


def copy_file(src, dst):
    try:
        shutil.copyfile(src, dst)
        return 0
    except Exception as err:
        print("Copying file error: {0}".format(err))
        exit(-1)


def get_run(path):
    if os.path.isfile(os.path.join(path, "parameter_0.log")):
        runs = len(glob.glob1(path, "parameter*"))
        return str(runs)
    return "0"
