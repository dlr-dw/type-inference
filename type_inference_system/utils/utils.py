import argparse


def get_args():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-c', '--config',
        metavar='C',
        default='None',
        help='The Configuration file')
    argparser.add_argument(
        '-lr', '--learning_rate',
        metavar='l',
        default='None',
        help='The learning_rate for training')
    argparser.add_argument(
        '-l', '--lam',
        metavar='r',
        default='None',
        help='Lambda coefficient')
    argparser.add_argument(
        '-r', '--reg',
        metavar='n',
        default='None',
        help='regularisation')
    argparser.add_argument(
        '-th', '--typehints',
        metavar='t',
        default='None',
        help='type hints')
    args = argparser.parse_args()
    return args
