import logging
from os.path import join

import numpy as np
import torch
import torch.nn as nn
from annoy import AnnoyIndex
from torch.autograd import Function


class ReverseLayerF(Function):
    @staticmethod
    def forward(ctx, x, alpha):
        ctx.alpha = alpha

        return x.view_as(x)

    @staticmethod
    def backward(ctx, grad_output):
        output = grad_output.neg() * ctx.alpha

        return output, None


class Feature_Extractor(nn.Module):
    def __init__(
        self,
        input_size: int,
        hidden_size: int,
        aval_type_size: int,
        num_layers: int,
        output_size: int,
        dropout_rate: float,
    ):
        super(Feature_Extractor, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.aval_type_size = aval_type_size
        self.num_layers = num_layers
        self.output_size = output_size

        self.lstm_id = nn.LSTM(
            self.input_size,
            self.hidden_size,
            self.num_layers,
            batch_first=True,
            bidirectional=True,
        )
        self.lstm_tok = nn.LSTM(
            self.input_size,
            self.hidden_size,
            self.num_layers,
            batch_first=True,
            bidirectional=True,
        )
        self.linear = nn.Linear(
            self.hidden_size * 2 * 2 + self.aval_type_size, self.output_size
        )
        # without tokens or id
        # self.linear = nn.Linear(self.hidden_size * 2 + self.aval_type_size, self.output_size)
        # without aval_type_size
        # self.linear = nn.Linear(self.hidden_size * 2 * 2, self.output_size)
        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, x_id, x_tok, x_type):
        # Using dropout on input sequences
        x_id = self.dropout(x_id)
        x_tok = self.dropout(x_tok)

        # Flattens LSTMs weights for data-parallelism in multi-GPUs config
        self.lstm_id.flatten_parameters()
        self.lstm_tok.flatten_parameters()

        x_id, _ = self.lstm_id(x_id)
        x_tok, _ = self.lstm_tok(x_tok)

        # Decode the hidden state of the last time step
        x_id = x_id[:, -1, :]
        x_tok = x_tok[:, -1, :]

        x = torch.cat((x_id, x_tok, x_type), 1)
        # without id
        # x = torch.cat((x_tok, x_type), 1)
        # without tokens
        # x = torch.cat((x_id, x_type), 1)
        # without aval_type_size
        # x = torch.cat((x_id, x_tok), 1)

        x = self.linear(x)
        return x


class Domain_Discriminator(nn.Module):
    def __init__(
        self, input_size: int, dropout_rate: float, total_epochs: float, device
    ):
        super(Domain_Discriminator, self).__init__()

        self.input_size = input_size
        self.linear1 = torch.nn.Linear(input_size, 1024)
        self.linear2 = torch.nn.Linear(1024, 1024)
        self.linear3 = torch.nn.Linear(1024, 2)
        self.relu = torch.nn.ReLU()
        self.sig = torch.nn.Sigmoid()
        self.device = device
        self.total_epochs = total_epochs

        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, feats, alpha):
        # feats = self.noise(epoch, feats)
        reverse_feats = ReverseLayerF.apply(feats, alpha)
        lin1 = self.linear1(reverse_feats)
        lin1 = self.relu(lin1)
        lin2 = self.linear2(lin1)
        lin2 = self.relu(lin2)
        y_logits = self.linear3(lin2)
        y_sig = self.sig(y_logits)
        return y_logits, y_sig

    def noise(self, epoch, data):
        sigma = torch.tensor(
            0.1 - 0.1 * np.clip(epoch / self.total_epochs,
                                a_min=0.0, a_max=1.0),
            dtype=torch.float32,
            requires_grad=False,
        ).to(self.device)
        return data + torch.normal(0.0, 1.0, size=data.shape).to(self.device) * sigma


class Unknown_Class_Detector(nn.Module):
    def __init__(self, input_size: int, dropout_rate: float, device):
        super(Unknown_Class_Detector, self).__init__()

        self.input_size = input_size
        self.linear1 = torch.nn.Linear(input_size, 1024)
        self.linear2 = torch.nn.Linear(1024, 1024)
        self.linear3 = torch.nn.Linear(1024, 2)
        self.relu = torch.nn.ReLU()
        self.sig = torch.nn.Sigmoid()
        self.device = device
        self.dropout = nn.Dropout(p=dropout_rate)

    def forward(self, feats):
        feats = self.dropout(feats)
        lin1 = self.linear1(feats)
        lin1 = self.relu(lin1)
        lin2 = self.linear2(lin1)
        lin2 = self.relu(lin2)
        out = self.linear3(lin2)
        out_sig = self.sig(out)
        return out, out_sig


class TripletModel(nn.Module):
    def __init__(self, device, config):
        super(TripletModel, self).__init__()
        self.W2V_VEC_LENGTH = 100
        self.device = device
        self.AVAILABLE_TYPES_NUMBER = 1024
        self.config = config
        self.epoch = 0
        logging.info("Build model...")
        self.build_model()
        logging.info("Built model")

        if self.config.pretrained_model:
            self.load(path=self.config.model_path)
            logging.info("Loaded pretrained model")

    def forward(self, a, p, n, alpha=0.0):
        a_e = self.feature_extractor(*(s.to(self.device) for s in a))
        p_e = self.feature_extractor(*(s.to(self.device) for s in p))
        n_e = self.feature_extractor(*(s.to(self.device) for s in n))
        feat = a_e
        if self.config.trainings_mode == "dann":
            d_pred, d_sig = self.domain_discriminator(a_e, alpha)
            return feat, p_e, n_e, d_sig, d_pred
        elif self.config.trainings_mode == "source-only-ext":
            unk_dis, unk_dis_sig = self.unknown_class_detector(a_e)
            return feat, p_e, n_e, unk_dis, unk_dis_sig
        else:
            return feat, p_e, n_e

    def save(self, path=None):
        logging.info("Saving model...")
        if path:
            torch.save(self, join(path, f"type4py_model.pt"))
        else:
            torch.save(self, join(
                self.config.checkpoint_dir, f"type4py_model.pt"))
        logging.info("Model saved")

    def load(self, path=None):
        if path:
            logging.info(
                "Load model: {}".format(
                    join(path, "checkpoint", f"type4py_model.pt"))
            )
            return torch.load(join(path, "checkpoint", f"type4py_model.pt"))
        else:
            logging.info(
                "Load model: {}".format(
                    join(self.config.checkpoint_dir, f"type4py_model.pt")
                )
            )
            return torch.load(join(self.config.checkpoint_dir, f"type4py_model.pt"))

    def set_epoch(self, epoch):
        self.epoch = epoch

    def build_model(self):
        self.feature_extractor = Feature_Extractor(
            self.W2V_VEC_LENGTH,
            self.config.hidden_size,
            self.AVAILABLE_TYPES_NUMBER,
            self.config.layers,
            self.config.output_size,
            self.config.dropout_rate,
        )
        if self.config.trainings_mode == "source-only-ext":
            self.unknown_class_detector = Unknown_Class_Detector(
                self.config.output_size, self.config.dropout_rate, self.device
            )
        else:
            self.domain_discriminator = Domain_Discriminator(
                self.config.output_size,
                self.config.dropout_rate,
                self.config.num_epochs,
                self.device,
            )


def create_knn_index(
    train_types_embed: np.array,
    valid_types_embed: np.array,
    type_embed_dim: int,
    tree_size: int,
) -> AnnoyIndex:
    """
    Creates KNNs index for given type embedding vectors
    """

    annoy_idx = AnnoyIndex(type_embed_dim, "euclidean")

    for i, v in enumerate(train_types_embed):
        annoy_idx.add_item(i, v)

    if valid_types_embed is not None:
        for i, v in enumerate(valid_types_embed):
            annoy_idx.add_item(len(train_types_embed) + i, v)

    annoy_idx.build(tree_size)
    return annoy_idx
