import logging
import os
import pickle
from os.path import join

import numpy as np
from base.base_eval import BaseEval
from models.triplet_model import create_knn_index


class Standard_Evaluator(BaseEval):
    def __init__(self, data, device, config, logger):
        super(Standard_Evaluator, self).__init__(
            data, device, config, logger)

    def save_features(self, features, names):
        for feature, name in zip(features, names):
            np.save(
                os.path.join(self.config.checkpoint_dir,
                             name),
                feature,
            )

    def predict_test_data(self, train_data, train_labels, test_data):
        embedded_data = []
        if len(train_data) == 2:
            annoy_index = create_knn_index(
                train_data[0],
                train_data[1],
                train_data[0].shape[1],
                20,
            )
        else:
            annoy_index = create_knn_index(
                train_data[0],
                None,
                train_data[0].shape[1],
                20,
            )
        for test_set in test_data:
            (
                pred_embed,
                pred_test_score,
                pred_types_distance,
            ) = self.predict_type_embed(
                test_set,
                train_labels,
                annoy_index,
                self.config.k,
            )
            embedded_data.append(pred_embed)
        return embedded_data

    def evaluate_test_data(
            self):
        logging.info("Start evaluation of the test data")
        embed_source, label_source, embed_target, label_target = self.data.get_embedded_data()
        n = [1, 10]

        train_type_embed, valid_type_embed, test_type_embed = (
            embed_source[0],
            embed_source[1],
            embed_source[2],
        )
        train_labels, valid_labels, test_labels = (
            label_source[0],
            label_source[1],
            label_source[2],
        )
        train_type_embed_target, valid_type_embed_target, test_type_embed_target = (
            embed_target[0],
            embed_target[1],
            embed_target[2],
        )
        (
            train_labels_target,
            valid_labels_target,
            test_labels_target,
        ) = (label_target[0], label_target[1], label_target[2])

        logging.info("Start predicting test data...")
        if self.config.trainings_mode == "target-only":
            pred_test, pred_test_target = self.predict_test_data([train_type_embed_target, valid_type_embed_target], np.concatenate((train_labels_target,
                                                                                                                                     valid_labels_target)), [test_type_embed, test_type_embed_target])
        else:
            pred_test, pred_test_target = self.predict_test_data([train_type_embed, valid_type_embed], np.concatenate(
                (train_labels, valid_labels)), [test_type_embed, test_type_embed_target])
        logging.info("Predicted test data")

        if self.config.trainings_mode == "combined":
            raise NotImplementedError(
                "Evaluation for combined training is not implemented!"
            )

        le_all = pickle.load(
            open(join(self.config.path_source_dataset,
                 "label_encoder_all.pkl"), "rb")
        )

        for top_n in n:
            log_summary = {}
            for eval_step in ["src", "trg"]:
                pred, labels = (pred_test, test_labels) if eval_step == "src" else (
                    pred_test_target, test_labels_target)

                log_summary = self.eval_type_embed(
                    pred,
                    labels,
                    self.data.common_types,
                    log_summary,
                    eval_step,
                    top_n=top_n,
                )

                pred_inv = np.array(
                    [le_all.inverse_transform(p)
                     for p in pred], dtype=object
                )
                labels_inv = np.array(
                    [le_all.inverse_transform([t])[0]
                     for t in labels]
                )
                common_types_inv = np.array(
                    [le_all.inverse_transform([c])[0]
                     for c in self.data.common_types]
                )

                if self.config.save_features:
                    self.save_features([pred_inv, labels_inv], [
                        f"pred_test_embed_inv_{eval_step}.npy", f"embed_test_labels_inv_{eval_step}.npy"])

                log_summary = self.eval_parametric_match(
                    pred_inv,
                    labels_inv,
                    common_types_inv,
                    log_summary,
                    eval_step,
                    top_n,
                )

                log_summary = self.calculate_mrr_score(
                    pred_inv,
                    labels_inv,
                    common_types_inv,
                    log_summary,
                    eval_step,
                    top_n,
                )
            log_summary.update({"eval_top_n": top_n})
            self.result_log.append(log_summary)
        self.logger.log(self.result_log, test_results=True)
        self.logger.write_log()
        logging.info("Finished evaluation of the test data")
