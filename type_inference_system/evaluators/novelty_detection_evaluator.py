import logging
import os

import numpy as np
import pandas as pd
from base.base_eval import BaseEval
from models.triplet_model import create_knn_index
from sklearn.metrics import f1_score


class Novelty_Detection_Evaluator(BaseEval):
    def __init__(self, data, device, config, logger):
        super(Novelty_Detection_Evaluator, self).__init__(
            data, device, config, logger)

    def calculate_cluster_centers(self, train_type_embed, train_labels):
        labels = train_labels
        features = train_type_embed
        self.clustercenters = {}
        for lab in np.unique(labels):
            indexes = np.where(labels == lab)
            cluster = features[indexes]
            self.clustercenters[lab] = np.mean(cluster, axis=0)
        vals = np.array(list(self.clustercenters.values()))
        self.index = create_knn_index(vals, None, vals.shape[1], 20)

        # validation for deciding the threshold
    def determmine_threshold(self, valid_type_embed,  valid_labels, output_path):
        dfdistance = pd.DataFrame(index=list(range(len(valid_type_embed))), columns=[
                                  'label', 'predictable', 'prediction', 'distance1_2', 'closest'])
        predictablelabels = list(self.clustercenters.keys())
        for i in range(len(valid_type_embed)):
            label = valid_labels[i]
            dfdistance.at[i, 'label'] = label
            if label in predictablelabels:
                dfdistance.at[i, 'predictable'] = 1
            else:
                dfdistance.at[i, 'predictable'] = 0
            tup = self.index.get_nns_by_vector(
                valid_type_embed[i], 3, search_k=-1, include_distances=True)
            dfdistance.at[i, 'distance1_2'] = tup[1][1] - tup[1][0]
            dfdistance.at[i, 'closest'] = predictablelabels[tup[0][0]]

        dfdistance['testing'] = dfdistance['label']
        dfdistance['testing'][dfdistance['predictable'] == 0] = 999999

        array = np.arange(0, 1, 0.001)
        dftesting = pd.DataFrame(index=array, columns=['f1', 'thresholdscore'])
        for q in array:
            self.thresholdscore = dfdistance['distance1_2'][dfdistance['predictable'] == 1].quantile(
                q)
            dftesting.at[q, 'thresholdscore'] = self.thresholdscore
            dfdistance['prediction'] = 999999
            dfdistance['prediction'][dfdistance['distance1_2'] >
                                     self.thresholdscore] = dfdistance['closest'][dfdistance['distance1_2'] > self.thresholdscore]
            true = pd.to_numeric(dfdistance['testing'])
            pred = pd.to_numeric(dfdistance['prediction'])
            dftesting.at[q, 'f1'] = f1_score(true, pred, average='micro')

        dftesting['f1'] = pd.to_numeric(dftesting['f1'])
        dftesting.to_csv(output_path + 'dftestingvalid.csv')
        self.thresholdscore = float(
            dftesting['thresholdscore'].loc[dftesting[['f1']].idxmax()])
        dfdistance['distance1_2'] = pd.to_numeric(dfdistance['distance1_2'])
        dfdistance['prediction'] = 999999
        dfdistance['prediction'][dfdistance['distance1_2'] >
                                 self.thresholdscore] = dfdistance['closest'][dfdistance['distance1_2'] > self.thresholdscore]
        dfdistance['prediction'] = pd.to_numeric(dfdistance['prediction'])
        dfdistance.to_csv(output_path + 'dfdistancenewvalid.csv')

    def prediction_test(self, test_type_embed, test_labels, path):
        dfdistance = pd.DataFrame(index=list(range(len(test_type_embed))), columns=[
                                  'label', 'predictable', 'prediction', 'distance1_2', 'closest'])
        predictablelabels = list(self.clustercenters.keys())
        for i in range(len(test_type_embed)):
            label = test_labels[i]
            dfdistance.at[i, 'label'] = label
            if label in predictablelabels:
                dfdistance.at[i, 'predictable'] = 1
            else:
                dfdistance.at[i, 'predictable'] = 0
            tup = self.index.get_nns_by_vector(
                test_type_embed[i], 3, search_k=-1, include_distances=True)
            dfdistance.at[i, 'distance1_2'] = tup[1][1] - tup[1][0]
            dfdistance.at[i, 'closest'] = predictablelabels[tup[0][0]]
        dfdistance['testing'] = dfdistance['label']
        dfdistance['testing'][dfdistance['predictable'] == 0] = 999999
        dfdistance['distance1_2'] = pd.to_numeric(dfdistance['distance1_2'])
        dfdistance['prediction'] = 999999
        dfdistance['prediction'][dfdistance['distance1_2'] >
                                 self.thresholdscore] = dfdistance['closest'][dfdistance['distance1_2'] > self.thresholdscore]

        dfdistance.to_csv(os.path.join(path, 'dfdistancetest.csv'))

        return dfdistance

    def predictables_evaluation(self, dfdistance):
        dfdistance['prediction'] = pd.to_numeric(dfdistance['prediction'])
        dfdistance['label'] = pd.to_numeric(dfdistance['label'])
        dfdistance['testing'] = pd.to_numeric(dfdistance['testing'])
        pred = dfdistance['prediction']
        true = dfdistance['label']
        f1general = f1_score(true, pred, average='micro')
        pred = dfdistance['prediction']
        true = dfdistance['testing']
        f1testing = f1_score(true, pred, average='micro')
        pred = dfdistance['prediction'][dfdistance['prediction'] != 999999]
        true = dfdistance['label'][dfdistance['prediction'] != 999999]
        f1predictables = f1_score(true, pred, average='micro')
        predictionrates = len(
            dfdistance[dfdistance['prediction'] != 999999])/len(dfdistance)
        dfdistance['Accurate'] = 0
        for i in range(len(dfdistance['Accurate'])):
            if (dfdistance['closest'].iloc[i] == dfdistance['testing'].iloc[i]):
                dfdistance['Accurate'].iloc[i] = 1
        log_summary = {}
        log_summary.update(
            {'f1_prediction_all': f1general,
             'f1_prediction_test': f1testing,
             'f1_prediction_above_threshold': f1predictables,
             'prediction_precentage': predictionrates,
             'prediction_unperictable_precentage': len(dfdistance[(dfdistance['prediction'] != 999999) & (dfdistance['predictable'] == 0)])/len(dfdistance),
             'prediction_non-accurate-predictable_precentage': len(dfdistance[(dfdistance['prediction'] != 999999) & (dfdistance['Accurate'] == 0) & (dfdistance['predictable'] == 1)])/len(dfdistance),
             'prediction_accurate_precentage': len(dfdistance[(dfdistance['prediction'] != 999999) & (dfdistance['Accurate'] == 1)])/len(dfdistance),
             'non-prediction_precentage': len(dfdistance[dfdistance['prediction'] == 999999])/len(dfdistance),
             'non-prediction_unperdictable_precentage': len(dfdistance[(dfdistance['prediction'] == 999999) & (dfdistance['predictable'] == 0)])/len(dfdistance),
             'non-prediction_non-accurate-predictable_precentage': len(dfdistance[(dfdistance['prediction'] == 999999) & (dfdistance['Accurate'] == 0) & (dfdistance['predictable'] == 1)])/len(dfdistance),
             'non-prediction_accurate_precentage': len(dfdistance[(dfdistance['prediction'] == 999999) & (dfdistance['Accurate'] == 1)])/len(dfdistance)
             }
        )
        return log_summary

    def evaluate_test_data(self):
        logging.info("Start evaluation of the test data")
        embed_source, label_source, embed_target, label_target = self.data.get_embedded_data()
        train_type_embed, valid_type_embed, test_type_embed = (
            embed_source[0],
            embed_source[1],
            embed_source[2],
        )
        train_labels, valid_labels, test_labels = (
            label_source[0],
            label_source[1],
            label_source[2],
        )
        train_type_embed_target, valid_type_embed_target, test_type_embed_target = (
            embed_target[0],
            embed_target[1],
            embed_target[2],
        )
        (
            train_labels_target,
            valid_labels_target,
            test_labels_target,
        ) = (label_target[0], label_target[1], label_target[2])

        if self.config.trainings_mode == "target-only":
            self.calculate_cluster_centers(
                train_type_embed_target, train_labels_target)
            self.determmine_threshold(
                valid_type_embed_target, valid_labels_target, self.config.checkpoint_dir)
        else:
            self.calculate_cluster_centers(train_type_embed, train_labels)
            self.determmine_threshold(
                valid_type_embed, valid_labels, self.config.checkpoint_dir)

        dfdistance = self.prediction_test(test_type_embed_target,
                                          test_labels_target, self.config.checkpoint_dir)
        log_summary = self.predictables_evaluation(dfdistance)
        log_summary.update({"eval_top_n": 1})
        self.logger.log([log_summary], test_results=True)
        self.logger.write_log()
        logging.info("Finished evaluation of the test data")
