import logging
import os
import sys
import traceback

import torch

from data_loader.data_generator import DataGenerator
from evaluators.novelty_detection_evaluator import Novelty_Detection_Evaluator
from evaluators.standard_evaluator import Standard_Evaluator
from models.triplet_model import TripletModel
from trainers.dann_trainer import DANNTrainer
from trainers.standard_trainer import StandardTrainer
from trainers.wdgrl_trainer import WDGRLTrainer
from utils.config import process_config
from utils.dirs import copy_file, create_dirs, get_run, remove_dirs
from utils.logger import Logger
from utils.utils import get_args


def main():
    try:
        args = get_args()
        config = process_config(args)
    except FileNotFoundError:
        print("No valid config file found: ", traceback.format_exc())
        exit(-1)
    except:
        print("Unexpected error: ", traceback.format_exc())
        logging.error(str(traceback.format_exc))
        logging.error(sys.exc_info())
        exit(-1)

    create_dirs([config.summary_dir, config.checkpoint_dir])
    config.run = get_run(config.summary_dir)
    copy_file(args.config, config.summary_dir + f"config_{config.run}.json")
    logger = Logger(config)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logging.info("Found {} device(s): {}".format(
        torch.cuda.device_count(), device))
    data = DataGenerator(config)

    if config.novelty_detection:
        evaluator = Novelty_Detection_Evaluator(data, device, config, logger)
    else:
        evaluator = Standard_Evaluator(data, device, config, logger)

    if config.trainings_mode == "dann":
        model = TripletModel(device, config)
        trainer = DANNTrainer(model, data, config, device, logger, evaluator)
    elif config.trainings_mode == "wdgrl":
        model = TripletModel(device, config)
        trainer = WDGRLTrainer(model, data, config, device, logger, evaluator)
    else:
        model = TripletModel(device, config)
        trainer = StandardTrainer(
            model, data, config, device, logger, evaluator)

    if config.trainings_mode != "eval-only":
        trainer.train()

    if config.save_best_model:
        model.save()

    evaluator.evaluate_test_data()

    if not os.listdir(config.checkpoint_dir):
        remove_dirs([config.checkpoint_dir])


if __name__ == "__main__":
    main()
