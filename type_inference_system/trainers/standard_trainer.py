import logging
from time import time

import torch
import torch.nn as nn
from base.base_train import BaseTrain


class StandardTrainer(BaseTrain):
    def __init__(self, model, data, config, device, logger, evaluator):
        super(StandardTrainer, self).__init__(data, config, logger)
        self.model = model
        self.evaluator = evaluator
        self.device = device
        self.criterion = torch.nn.TripletMarginLoss(margin=self.config.margin)
        self.optimizer = torch.optim.Adam(
            model.feature_extractor.parameters(),
            lr=self.config.learning_rate,
            weight_decay=self.config.weight_decay,
        )
        self.data_parallel = False
        logging.info("Initialized Standard-Trainer")

    def train(self):
        self.best_val_acc = 0
        self.last_improve = 0
        if torch.cuda.device_count() > 1:
            self.model = nn.DataParallel(self.model)
            self.data_parallel = True
        self.model = self.model.to(self.device)
        logging.info("Start training...")
        training_t = time()
        for epoch in range(1, self.config.num_epochs + 1):
            if self.train_epoch(epoch) == 1:
                break

        logging.info("Finished training after %.2f min" %
                     ((time() - training_t) / 60))
        if self.config.early_stop > 0 and self.config.do_validation:
            if self.data_parallel:
                self.model = self.model.module.load()
            else:
                self.model = self.model.load()

        embed_source, label_source, embed_target, label_target = self.get_embeddings(
            self.model, self.device
        )

        self.data.set_embedded_data(
            embed_source, label_source, embed_target, label_target)

    def train_epoch(self, epoch):
        self.model.train()
        epoch_start_t = time()
        total_loss = 0
        target_data_iter = iter(self.data.train_loader_target)

        for batch_i, (anchor, positive_ex, negative_ex) in enumerate(
            self.data.train_loader
        ):
            try:
                anchor_t, positive_ex_t, negative_ex_t = next(target_data_iter)
            except StopIteration:
                target_data_iter = iter(self.data.train_loader_target)
                (anchor_t, positive_ex_t, negative_ex_t) = next(target_data_iter)
            loss_c = self.train_step(
                anchor, positive_ex, negative_ex, anchor_t, positive_ex_t, negative_ex_t
            )
            total_loss += loss_c
        logging.info(
            f"epoch: {epoch} train loss: {total_loss} in {(time() - epoch_start_t) / 60.0:.2f} min."
        )

        if self.config.do_validation:
            valid_start = time()
            (
                valid_loss,
                valid_loss_target,
                valid_all_acc,
                valid_all_acc_target,
                valid_co_acc,
                valid_co_acc_target,
                valid_ra_acc,
                valid_ra_acc_target,
                insec_acc,
                insec_acc_t,
            ) = self.evaluator.validation(self.model, self.criterion)

            logging.info(
                f"epoch: {epoch} valid loss source: {valid_loss}, valid loss target {valid_loss_target} in {(time() - valid_start) / 60.0:.2f} min."
            )
            summaries_dict = {
                "total_loss": total_loss,
                "valid_triplet_loss_source": valid_loss,
                "valid_triplet_loss_target": valid_loss_target,
                "valid_acc_source": valid_all_acc,
                "valid_acc_target": valid_all_acc_target,
                "valid_common_acc_source": valid_co_acc,
                "valid_common_acc_target": valid_co_acc_target,
                "valid_rare_acc_source": valid_ra_acc,
                "valid_rare_acc_target": valid_ra_acc_target,
                "valid_insec_acc_source": insec_acc,
                "valid_insec_acc_target": insec_acc_t,
            }
            self.logger.log(summaries_dict)
            if self.config.early_stop != 0:
                if valid_all_acc > self.best_val_acc:
                    self.best_val_acc = valid_all_acc
                    self.last_improve = 0
                    if self.data_parallel:
                        self.model.module.save()
                    else:
                        self.model.save()
                else:
                    self.last_improve += 1
                if self.last_improve > self.config.early_stop:
                    logging.info("Early stop after {} epochs".format(epoch))
                    return 1
        else:
            summaries_dict = {"total_loss": total_loss}
            self.logger.log(summaries_dict)
        return 0

    def train_step(
        self, anchor, positive_ex, negative_ex, anchor_t, positive_ex_t, negative_ex_t
    ):
        anchor, _ = anchor[0], anchor[1]
        positive_ex, _ = positive_ex[0], positive_ex[1]
        negative_ex, _ = negative_ex[0], negative_ex[1]

        anchor_t, _ = anchor_t[0], anchor_t[1]
        positive_ex_t, _ = positive_ex_t[0], positive_ex_t[1]
        negative_ex_t, _ = negative_ex_t[0], negative_ex_t[1]

        self.optimizer.zero_grad()
        loss = 0

        if (
            self.config.trainings_mode == "source-only"
            or self.config.trainings_mode == "combined"
        ):
            anchor_embed, positive_ex_embed, negative_ex_embed = self.model(
                anchor, positive_ex, negative_ex
            )
            loss += self.criterion(anchor_embed,
                                   positive_ex_embed, negative_ex_embed)

        if (
            self.config.trainings_mode == "target-only"
            or self.config.trainings_mode == "combined"
        ):
            anchor_embed, positive_ex_embed, negative_ex_embed = self.model(
                anchor_t, positive_ex_t, negative_ex_t
            )
            loss += self.criterion(anchor_embed,
                                   positive_ex_embed, negative_ex_embed)

        loss.backward()
        self.optimizer.step()

        return loss.item()
