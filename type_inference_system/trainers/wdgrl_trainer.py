import logging
from time import time

import torch
import torch.nn as nn
from base.base_train import BaseTrain
from torch.autograd import grad

EPS = 1e-10


class WDGRLTrainer(BaseTrain):
    def __init__(self, model, data, config, device, logger, evaluator):
        super(WDGRLTrainer, self).__init__(data, config, logger)
        self.model = model
        self.evaluator = evaluator
        self.device = device
        self.data_parallel = False
        self.critic = nn.Sequential(
            nn.Linear(self.config.output_size, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 1),
        ).to(device)
        self.criterion = torch.nn.TripletMarginLoss(margin=self.config.margin).to(
            device
        )
        self.optimizer = torch.optim.Adam(
            model.feature_extractor.parameters(), lr=self.config.learning_rate
        )
        self.optimizer_critic = torch.optim.Adam(
            self.critic.parameters(), lr=self.config.learning_rate
        )
        logging.info("Initialized Dann-Trainer")

    def set_requires_grad(self, model, requires_grad=True):
        for param in model.parameters():
            param.requires_grad = requires_grad

    def gradient_penalty(self, critic, h_s, h_t):
        # based on: https://github.com/caogang/wgan-gp/blob/master/gan_cifar10.py#L116
        alpha = torch.rand(h_s.size(0), 1).to(self.device)
        differences = h_t - h_s
        interpolates = h_s + (alpha * differences)
        interpolates = torch.stack([interpolates, h_s, h_t]).requires_grad_()

        preds = critic(interpolates)
        gradients = grad(
            preds,
            interpolates,
            grad_outputs=torch.ones_like(preds),
            retain_graph=True,
            create_graph=True,
        )[0]
        gradient_norm = gradients.norm(2, dim=1)
        gradient_penalty = ((gradient_norm - 1) ** 2).mean()
        return gradient_penalty

    def train(self):
        self.model = self.model.to(self.device)
        self.critic = self.critic.to(self.device)
        self.best_val_acc = 0
        self.last_improve = 0

        logging.info("Start training...")
        training_t = time()

        for epoch in range(1, self.config.num_epochs + 1):
            if self.train_epoch(epoch) == 1:
                break
        logging.info("Finished training after %.2f min" %
                     ((time() - training_t) / 60))
        if self.config.early_stop > 0 and self.config.do_validation:
            if self.data_parallel:
                self.model = self.model.module.load()
            else:
                self.model = self.model.load()
        embed_source, label_source, embed_target, label_target = self.get_embeddings(
            self.model, self.device
        )
        self.data.set_embedded_data(
            embed_source, label_source, embed_target, label_target)

    def train_epoch(self, epoch):
        self.model.train()
        epoch_start_t = time()
        sum_critic_loss = 0
        sum_wd_distance = 0
        sum_class_loss = 0
        target_data_iter = iter(self.data.train_loader_target)

        for batch_i, (anchor, positive_ex, negative_ex) in enumerate(
            self.data.train_loader
        ):
            try:
                anchor_t, positive_ex_t, negative_ex_t = next(target_data_iter)
            except StopIteration:
                target_data_iter = iter(self.data.train_loader_target)
                (anchor_t, positive_ex_t, negative_ex_t) = next(target_data_iter)

            critic_loss, wd_distance, class_loss = self.train_step(
                anchor, positive_ex, negative_ex, anchor_t, positive_ex_t, negative_ex_t
            )
            sum_critic_loss += critic_loss
            sum_wd_distance += wd_distance
            sum_class_loss += class_loss
        total_loss = sum_critic_loss + sum_wd_distance + sum_class_loss
        logging.info(
            f"epoch: {epoch} train loss: {total_loss} in {(time() - epoch_start_t) / 60.0:.2f} min."
        )
        if self.config.do_validation and epoch:
            valid_start = time()
            (
                valid_loss,
                valid_loss_target,
                valid_all_acc,
                valid_all_acc_target,
                valid_co_acc,
                valid_co_acc_target,
                valid_ra_acc,
                valid_ra_acc_target,
                insec_acc,
                insec_acc_t,
            ) = self.evaluator.validation(self.model, self.criterion)

            logging.info(
                f"epoch: {epoch} valid loss source: {valid_loss}, valid loss target {valid_loss_target} in {(time() - valid_start) / 60.0:.2f} min."
            )
            summaries_dict = {
                "total_loss": total_loss,
                "critic_loss": sum_critic_loss,
                "wasserstein_distance": sum_wd_distance,
                "classification_loss": sum_class_loss,
                "valid_triplet_loss_source": valid_loss,
                "valid_triplet_loss_target": valid_loss_target,
                "valid_all_acc_source": valid_all_acc,
                "valid_all_acc_target": valid_all_acc_target,
                "valid_common_acc_source": valid_co_acc,
                "valid_common_acc_target": valid_co_acc_target,
                "valid_rare_acc_source": valid_ra_acc,
                "valid_rare_acc_target": valid_ra_acc_target,
                "valid_insec_acc_source": insec_acc,
                "valid_insec_acc_target": insec_acc_t,
            }
            self.logger.log(summaries_dict)
            if self.config.early_stop != 0:
                if insec_acc_t > self.best_val_acc:
                    self.best_val_acc = insec_acc_t
                    self.last_improve = 0
                    if self.data_parallel:
                        self.model.module.save()
                    else:
                        self.model.save()
                else:
                    self.last_improve += 1
                if self.last_improve > self.config.early_stop:
                    logging.info("Early stop after {} epochs".format(epoch))
                    return 1
        else:
            summaries_dict = {
                "total_loss": total_loss,
                "critic_loss": sum_critic_loss,
                "wasserstein_distance": sum_wd_distance,
                "classification_loss": sum_class_loss,
            }
            self.logger.log(summaries_dict)

        return 0

    def train_step(
        self, anchor, positive_ex, negative_ex, anchor_t, positive_ex_t, negative_ex_t
    ):
        anchor, _ = anchor[0], anchor[1]
        positive_ex, _ = positive_ex[0], positive_ex[1]
        negative_ex, _ = negative_ex[0], negative_ex[1]

        anchor_t, _ = anchor_t[0], anchor_t[1]
        positive_ex_t, _ = positive_ex_t[0], positive_ex_t[1]
        negative_ex_t, _ = negative_ex_t[0], negative_ex_t[1]

        self.set_requires_grad(
            self.model.feature_extractor, requires_grad=False)
        self.set_requires_grad(self.critic, requires_grad=True)

        sum_critic_loss = 0
        sum_wd_distance = 0
        sum_class_loss = 0

        with torch.no_grad():
            h_s = self.model.feature_extractor(
                *(s.to(self.device) for s in anchor))
            h_t = self.model.feature_extractor(
                *(s.to(self.device) for s in anchor_t))

        for _ in range(self.config.k_critic):
            gp = self.gradient_penalty(self.critic, h_s, h_t)

            critic_s = self.critic(h_s)
            critic_t = self.critic(h_t)
            wasserstein_distance = critic_s.mean() - critic_t.mean()

            critic_cost = -wasserstein_distance + self.config.gamma * gp

            self.optimizer_critic.zero_grad()
            critic_cost.backward()
            self.optimizer_critic.step()

            sum_critic_loss += critic_cost.item()

        self.set_requires_grad(
            self.model.feature_extractor, requires_grad=True)
        self.set_requires_grad(self.critic, requires_grad=False)
        for _ in range(self.config.k_clf):
            anchor_embed, positive_ex_embed, negative_ex_embed = self.model(
                anchor, positive_ex, negative_ex
            )
            anchor_embed_t, _, _ = self.model(
                anchor_t, positive_ex_t, negative_ex_t)
            loss_c = self.criterion(
                anchor_embed, positive_ex_embed, negative_ex_embed)

            wasserstein_distance = (
                self.critic(anchor_embed).mean() -
                self.critic(anchor_embed_t).mean()
            )

            loss = loss_c + self.config.wd_clf * wasserstein_distance
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            sum_wd_distance += float(wasserstein_distance)
            sum_class_loss += loss_c.item()

        return (
            (sum_critic_loss / self.config.k_critic),
            (sum_wd_distance / self.config.k_clf),
            (sum_class_loss / self.config.k_clf),
        )
