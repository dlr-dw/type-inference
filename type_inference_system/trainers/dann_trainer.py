import logging
from time import time

import numpy as np
import torch
import torch.nn as nn
from base.base_train import BaseTrain

EPS = 1e-10


class Cross_Entropy_Confusion_Loss(torch.nn.Module):
    def __init__(self):
        super(Cross_Entropy_Confusion_Loss, self).__init__()

    def forward(self, x, y):
        return torch.clamp(
            torch.mean(-1 / 2 * torch.log((x * (1 - x)) + EPS)), min=0.0, max=3.0
        )


class Target_Confusion_Loss(torch.nn.Module):
    def __init__(self):
        super(Target_Confusion_Loss, self).__init__()

    def forward(self, x, y):
        return torch.clamp(
            torch.mean(y.float() * (-1 * torch.log(1.0 - x + EPS))), min=0.0, max=3.0
        )


class DANNTrainer(BaseTrain):
    def __init__(self, model, data, config, device, logger, evaluator):
        super(DANNTrainer, self).__init__(data, config, logger)
        self.model = model
        self.evaluator = evaluator
        self.device = device
        self.criterion = torch.nn.TripletMarginLoss(margin=self.config.margin).to(
            device
        )
        if config.optimizer == "adam":
            self.optimizer = torch.optim.Adam(
                model.parameters(),
                lr=self.config.learning_rate,
                weight_decay=self.config.weight_decay,
            )
            self.domain_optimizer = torch.optim.Adam(
                model.domain_discriminator.parameters(),
                lr=self.config.learning_rate,
                weight_decay=self.config.weight_decay,
            )
        else:
            self.optimizer = torch.optim.SGD(
                model.feature_extractor.parameters(),
                lr=self.config.learning_rate,
                momentum=0.9,
                weight_decay=self.config.weight_decay,
            )
            self.domain_optimizer = torch.optim.SGD(
                model.domain_discriminator.parameters(),
                lr=self.config.learning_rate,
                momentum=0.9,
                weight_decay=self.config.weight_decay,
            )
            self.scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
                self.optimizer, self.config.num_epochs
            )
            self.domain_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
                self.domain_optimizer, self.config.num_epochs
            )
        self.criterion_domain = torch.nn.CrossEntropyLoss().to(device)
        if self.config.confusion_loss == "ec":
            self.criterion_confusion = Cross_Entropy_Confusion_Loss().to(device)
        elif self.config.confusion_loss == "target":
            self.criterion_confusion = Target_Confusion_Loss().to(device)
        self.data_parallel = False
        logging.info("Initialized Dann-Trainer")

    def train(self):
        if torch.cuda.device_count() > 1:
            self.model = nn.DataParallel(self.model)
            self.data_parallel = True
        self.model = self.model.to(self.device)
        self.domain_label_source = self.data.domain_label_source.long().to(self.device)
        self.domain_label_target = self.data.domain_label_target.long().to(self.device)
        self.lambda_coef = torch.tensor(
            self.config.lambda_coef, dtype=torch.float32, requires_grad=False
        ).to(self.device)
        self.best_val_acc = 0
        self.last_improve = 0

        logging.info("Start training...")
        training_t = time()

        for epoch in range(1, self.config.num_epochs + 1):
            if self.data_parallel:
                self.model.module.set_epoch(epoch)
            else:
                self.model.set_epoch(epoch)
            if self.train_epoch(epoch) == 1:
                break
        logging.info("Finished training after %.2f min" %
                     ((time() - training_t) / 60))
        if self.config.early_stop > 0 and self.config.do_validation:
            if self.data_parallel:
                self.model = self.model.module.load()
            else:
                self.model = self.model.load()

        embed_source, label_source, embed_target, label_target = self.get_embeddings(
            self.model, self.device
        )
        self.data.set_embedded_data(
            embed_source, label_source, embed_target, label_target)

    def train_epoch(self, epoch):
        self.model.train()
        epoch_start_t = time()
        loss_c = 0
        loss_conf = 0
        loss_dm = 0
        self.correct = 0
        self.total_samples = 0
        target_data_iter = iter(self.data.train_loader_target)
        p = float(epoch) / self.config.num_epochs
        alpha = torch.tensor(
            2.0 / (1.0 + np.exp(-20 * p)) - 1, dtype=torch.float32, requires_grad=False
        ).to(self.device)

        for batch_i, (anchor, positive_ex, negative_ex) in enumerate(
            self.data.train_loader
        ):
            try:
                anchor_t, positive_ex_t, negative_ex_t = next(target_data_iter)
            except StopIteration:
                target_data_iter = iter(self.data.train_loader_target)
                (anchor_t, positive_ex_t, negative_ex_t) = next(target_data_iter)

            loss_c_te, loss_conf_te, loss_dm_te = self.train_step(
                anchor,
                positive_ex,
                negative_ex,
                anchor_t,
                positive_ex_t,
                negative_ex_t,
                alpha,
            )
            loss_c += loss_c_te
            loss_conf += loss_conf_te
            loss_dm += loss_dm_te
        total_loss = loss_c + loss_conf + loss_dm
        logging.info(
            f"epoch: {epoch} train loss: {total_loss} in {(time() - epoch_start_t) / 60.0:.2f} min."
        )
        if self.config.do_validation:

            valid_start = time()
            (
                valid_loss,
                valid_loss_target,
                valid_all_acc,
                valid_all_acc_target,
                valid_co_acc,
                valid_co_acc_target,
                valid_ra_acc,
                valid_ra_acc_target,
                insec_acc,
                insec_acc_t,
            ) = self.evaluator.validation(self.model, self.criterion)

            logging.info(
                f"epoch: {epoch} valid loss source: {valid_loss}, valid loss target {valid_loss_target} in {(time() - valid_start) / 60.0:.2f} min."
            )
            summaries_dict = {
                "total_loss": total_loss,
                "confusion_loss": loss_conf,
                "prediction_loss_source": loss_c,
                "domain_loss": loss_dm,
                "domain_acc": float((self.correct / self.total_samples)),
                "valid_triplet_loss_source": valid_loss,
                "valid_triplet_loss_target": valid_loss_target,
                "valid_all_acc_source": valid_all_acc,
                "valid_all_acc_target": valid_all_acc_target,
                "valid_common_acc_source": valid_co_acc,
                "valid_common_acc_target": valid_co_acc_target,
                "valid_rare_acc_source": valid_ra_acc,
                "valid_rare_acc_target": valid_ra_acc_target,
                "valid_insec_acc_source": insec_acc,
                "valid_insec_acc_target": insec_acc_t,
            }
            self.logger.log(summaries_dict)
            if self.config.early_stop != 0:
                if insec_acc_t > self.best_val_acc:
                    self.best_val_acc = insec_acc_t
                    self.last_improve = 0
                    if self.data_parallel:
                        self.model.module.save()
                    else:
                        self.model.save()
                else:
                    self.last_improve += 1
                if self.last_improve > self.config.early_stop:
                    logging.info("Early stop after {} epochs".format(epoch))
                    return 1
        else:
            summaries_dict = {
                "total_loss": total_loss,
                "confusion_loss": loss_conf,
                "prediction_loss_source": loss_c,
                "domain_loss": loss_dm,
            }
            self.logger.log(summaries_dict)

        if self.config.optimizer == "sgd":
            self.scheduler.step()
            self.domain_scheduler.step()
        return 0

    def train_step(
        self,
        anchor,
        positive_ex,
        negative_ex,
        anchor_t,
        positive_ex_t,
        negative_ex_t,
        alpha,
    ):
        anchor, _ = anchor[0], anchor[1]
        positive_ex, _ = positive_ex[0], positive_ex[1]
        negative_ex, _ = negative_ex[0], negative_ex[1]

        anchor_t, _ = anchor_t[0], anchor_t[1]
        positive_ex_t, _ = positive_ex_t[0], positive_ex_t[1]
        negative_ex_t, _ = negative_ex_t[0], negative_ex_t[1]

        (
            anchor_embed,
            positive_ex_embed,
            negative_ex_embed,
            domain_c,
            domain_pred,
        ) = self.model(anchor, positive_ex, negative_ex, alpha=alpha)
        (
            _,
            _,
            _,
            domain_c_t,
            domain_pred_t,
        ) = self.model(anchor_t, positive_ex_t, negative_ex_t, alpha=alpha)

        self.optimizer.zero_grad()
        loss_c = self.criterion(
            anchor_embed, positive_ex_embed, negative_ex_embed)
        loss_dm_src = self.criterion_domain(
            domain_pred, self.domain_label_source[: len(domain_pred)]
        )
        loss_dm_tgt = self.criterion_domain(
            domain_pred_t, self.domain_label_target[: len(domain_pred_t)]
        )
        loss = loss_c + loss_dm_src + loss_dm_tgt
        loss.backward()

        self.correct += (
            torch.eq(
                torch.argmax(
                    domain_c, 1), self.domain_label_source[: len(domain_c)]
            )
            .sum()
            .item()
        )
        self.correct += (
            torch.eq(
                torch.argmax(
                    domain_c_t, 1), self.domain_label_target[: len(domain_c_t)]
            )
            .sum()
            .item()
        )
        self.total_samples += len(domain_c) + len(domain_c_t)
        self.optimizer.step()

        return loss_c.item(), 0, 0
