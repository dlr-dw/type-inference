# Type Inference Pipeline

In this project, we investigate the generalization ability of the state-of-the-art machine learning-based type inference system [Type4Py](https://dl.acm.org/doi/abs/10.1145/3510003.3510124). Therefore we use our [CrossDomainTypes4Py](https://doi.org/10.5281/zenodo.5747024) dataset and the benchmark dataset [ManyTypes4Py](https://ieeexplore.ieee.org/abstract/document/9463150). We address problems such as dataset shifts, out-of-vocabulary words, rare classes, and unknown classes. For more information, we refer to our [paper](https://arxiv.org/abs/2208.09189). Furthermore, we have implemented the [Tipical](https://ieeexplore.ieee.org/document/10197800) extension to identify new and unknown data types to prevent the system from becoming uncertain and generating inaccurate predictions.

## Installation 

* Python 3.8

**Run:**

```shell
    git clone git@gitlab.com:dlr-dw/type-inference.git
    cd <project directory>
    pip install -r requirements.txt

```

## Structure

### Folder: Repository Mining
The folder contains scripts to create your own dataset list with projects from the platforms GitHub and Libraries.

### Folder: Preprocessing
You can download and preprocess a dataset using a predefined or self-created dataset list. We used the [ManyTypes4Py pipeline](https://github.com/saltudelft/many-types-4-py-dataset) as a basis for the preprocessing steps and adapted it for the cross-domain setup.

### Folder: Type Inference System
This folder contains a state-of-the-art type inference system based on the implementation of [Type4Py](https://github.com/saltudelft/type4py). We exented the pipeline to support cross-domain experiments. Additionally, we added a simple novelty detection component.

## Citing CrossDomainTypes4Py and Tipical
```
@INPROCEEDINGS{10174049,
  author={Gruner, Bernd and Sonnekalb, Tim and Heinze, Thomas S. and Brust, Clemens-Alexander},
  booktitle={2023 IEEE/ACM 20th International Conference on Mining Software Repositories (MSR)}, 
  title={Cross-Domain Evaluation of a Deep Learning-Based Type Inference System}, 
  year={2023},
  volume={},
  number={},
  pages={158-169},
  doi={10.1109/MSR59073.2023.00034}
  }

```
```
@INPROCEEDINGS{10197800,
  author={Elkobi, Jonathan and Gruner, Bernd and Sonnekalb, Tim and Brust, Clemens-Alexander},
  booktitle={2023 IEEE/ACIS 21st International Conference on Software Engineering Research, Management and Applications (SERA)}, 
  title={TIPICAL - Type Inference for Python In Critical Accuracy Level}, 
  year={2023},
  volume={},
  number={},
  pages={16-21},
  doi={10.1109/SERA57763.2023.10197800}}
```
