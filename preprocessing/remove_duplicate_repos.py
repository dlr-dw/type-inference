import argparse
import os
import re
import shutil

from tqdm import tqdm

tqdm.pandas()


def get_repo_list(ds_path):
    subfolders = [f.path for f in os.scandir(ds_path) if f.is_dir()]
    all_projects = []
    for subfolder in subfolders:
        projects = [f.path for f in os.scandir(subfolder) if f.is_dir()]
        for project in projects:
            all_projects.append(re.findall(fr"{ds_path}/(.*)", project)[0])
    return all_projects


def remove_dirs(dirs):
    try:
        for dir_ in dirs:
            if os.path.exists(dir_):
                shutil.rmtree(dir_)
        return 0
    except Exception as err:
        print("Removing directories error: {0}".format(err))
        exit(-1)


def delete_repos(path, repo_list):
    rm_repos = []
    subfolders = [f.path for f in os.scandir(path) if f.is_dir()]
    for subfolder in subfolders:
        projects = [f.path for f in os.scandir(subfolder) if f.is_dir()]
        for project in projects:
            if not re.findall(fr"{path}/(.*)", project)[0] in repo_list:
                rm_repos.append(os.path.join(
                    path, re.findall(fr"{path}/(.*)", project)[0]))
    print(f"{len(rm_repos)} repositories found for removal")
    remove_dirs(rm_repos)


def main():
    """Deletes duplicate repositories between the two datasets."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-f', required=True, type=str,
                            help="Path to first dataset")
    arg_parser.add_argument('-s', required=True,
                            type=str, help="Path to second dataset")
    args = arg_parser.parse_args()

    ds1_repos = set(get_repo_list(args.f))
    ds2_repos = set(get_repo_list(args.s))
    intersection_repos = list((ds1_repos).intersection((ds2_repos)))

    print("Number of repos in dataset 1: ", len(ds1_repos))
    print("Number of repos in dataset 2: ", len(ds2_repos))
    print("Number of intersection repos: ", len(intersection_repos))
    print("Dataset 1 and dataset 2 are disjoint: ",
          ds1_repos.isdisjoint(ds2_repos))

    ds1_repos.difference_update(
        set(intersection_repos[: len(intersection_repos)//2]))
    ds2_repos.difference_update(
        intersection_repos[len(intersection_repos)//2:])

    with open('ds1_repos.txt', 'w') as f:
        for repo in ds1_repos:
            f.write(f'{repo}\n')

    with open('ds2_repos.txt', 'w') as f:
        for repo in ds2_repos:
            f.write(f'{repo}\n')

    with open('ds1_repos.txt', 'r') as f:
        ds1_repos = f.readlines()
    ds1_repos = [repo.replace("\n", "") for repo in ds1_repos]

    with open('ds2_repos.txt', 'r') as f:
        ds2_repos = f.readlines()
    ds2_repos = [repo.replace("\n", "") for repo in ds2_repos]

    delete_repos(args.f, ds1_repos)
    delete_repos(args.s, ds2_repos)

    ds1_repos = set(get_repo_list(args.f))
    ds2_repos = set(get_repo_list(args.s))
    intersection_repos = list((ds1_repos).intersection((ds2_repos)))

    print("Number of repos in dataset 1: ", len(ds1_repos))
    print("Number of repos in dataset 2: ", len(ds2_repos))
    print("Number of intersection repos: ", len(intersection_repos))
    print("Dataset 1 and dataset 2 are disjoint: ",
          ds1_repos.isdisjoint(ds2_repos))


if __name__ == "__main__":
    main()
