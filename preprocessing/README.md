# Preprocessing Steps 

The described preprocessing steps are necessary to prepare CrossDomainTypes4Py (or your own dataset) for machine learning-based type inference systems. We recommend using the described folder structure.

## Required Folder Structure

```
preprocessing
|  preprocessing scripts
|
|__tokens
|
|__data
   |__dataset 1
   |   |__raw
   |   |__preprocessed dataset
   |      |     some preprocessing artifacts
   |      |
   |      |__encoded dataset
   |__dataset 2
   |   |__raw
   |   |__preprocessed dataset
   |      |     some preprocessing artifacts
   |      |
   |      |__encoded dataset

```

## General Preprocessing Steps

1. Download the dataset lists from [Zenodo](https://doi.org/10.5281/zenodo.5747024) or use your own dataset list
   
2. Clone repositories from the dataset list

   ```shell
   python repo_cloner.py -r <Path to Dataset Spec File> -o <Dataset Directory>

   python repo_cloner.py -r dataset_1.spec -o data/dataset_1/raw
   python repo_cloner.py -r dataset_2.spec -o data/dataset_2/raw
   ```

3. Reset repositories to commit (if necessary)

   ```shell
   ./reset_commits.sh  <Path to Dataset Spec File> <Dataset Directory>

   ./reset_commits.sh  dataset_1.spec data/dataset_1/raw
   ./reset_commits.sh  dataset_2.spec data/dataset_2/raw
   ```

4. Delete duplicate repositories
   ```shell
   python remove_duplicate_repos.py -f <Path to Dataset 1> -s <Path to Dataset 2>

   python remove_duplicate_repos.py -f data/dataset_1/raw -s data/dataset_2/raw
   ```

5. Delete all non-python files and empty directories
   ```shell
   cd data/dataset_1/raw
   find . -type f -not -name '*.py' -delete
   find . -type d -empty -delete

   cd data/dataset_2/raw
   find . -type f -not -name '*.py' -delete
   find . -type d -empty -delete
   ```

6. Detect duplicate files
   ```shell
   cd4py --p <Dataset Directory> --ot <Token Directory> --od dataset_duplicates.jsonl.gz --d 1024

   cd4py --p data/ --ot tokens --od dataset_duplicates.jsonl.gz --d 1024
   ```
   * the token directory can be removed after step 6


7. Delete duplicate files
   ```shell
   python delete_duplicates.py -d dataset_duplicates.jsonl.gz -o <Filename (Removed Duplicates)> 

   python delete_duplicates.py -d dataset_duplicates.jsonl.gz -o deleted_repositories.txt 
   ```

8.  Split the dataset into training, validation, and test on project-level
      ```shell
      python split_ds.py <Dataset 1 Directory> --od <Output CSV File>

      python split_ds.py data/dataset_1/raw --od dataset_1_split.csv
      python split_ds.py data/dataset_2/raw --od dataset_2_split.csv
      ```
       * for a split on file-level skip this step and execute step 9 without the argument --s

9.  Extract information
      ```shell
      libsa4py process --p <Path to dataset> --o <Processed Project Path> --s <Output CSV File> --j <Workers>

      libsa4py process --p data/dataset_1/raw --o data/dataset_1/preprocessed_dataset --s dataset_1_split.csv --j 8
      libsa4py process --p data/dataset_2/raw --o data/dataset_2/preprocessed_dataset --s dataset_2_split.csv --j 8
      ```

## Type4Py Data Preparation Steps

10. Prepare extracted information
      ```shell
      python preprocess.py <Path to dataset 1> <Short name dataset 1> <Path to dataset 2> <Short name dataset 2> <folder name> --parametric_types

      python preprocess.py data/dataset_1/preprocessed_dataset domain1 data/dataset_2/preprocessed_dataset domain2 encoded_dataset --parametric_types
      ```
      The short names of the dataset are used to name their type hints. They are needed in step 11 and in the config file.


11. Vectorize 

      ```shell
      python vectorize.py <Path to dataset 1> <Short name dataset 1> <Path to dataset 2> <Short name dataset 2> <Word2Vec model train mode>

      python vectorize.py data/dataset_1/preprocessed_dataset/encoded_dataset domain1 data/dataset_2/preprocessed_dataset/encoded_dataset domain2 optimistic
      ```

      We use three different training modes for the word2vec model. In the training modes, different sets are used to train the word2vec model:
      * realistic: training set from dataset 1
      * optimistic: training sets from both datasets
      * realistic: all sets from both datasets