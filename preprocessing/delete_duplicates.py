import argparse
import os
import random

from dpu_utils.utils.dataloading import load_jsonl_gz


def delete_duplicates(path_dup_list, output_dir, preferred):
    clusters = load_jsonl_gz(path_dup_list)
    files = []
    for cluster in clusters:
        pref_files = []
        for i in range(0, len(cluster)):
            if preferred in cluster[i]:
                pref_files.append(i)
        if not pref_files:
            files.append(cluster[random.randrange(len(cluster))])
        else:
            files.append(
                cluster[pref_files[random.randrange(len(pref_files))]])
    duplicate_files = [f for l in load_jsonl_gz(path_dup_list) for f in l]
    duplicate_files = set(duplicate_files).difference(set(files))

    with open(output_dir, "w") as out_file:
        out_file.write("\n".join(duplicate_files))
        out_file.write("\n")

    deleted_files = 0
    for f in duplicate_files:
        if not os.path.exists(f):
            continue
        try:
            os.remove(f)
            deleted_files += 1
        except Exception as e:
            print(f"Unexpected error: {e}")

    print(f"Deleted {deleted_files} duplicates:")


def main():
    """Uses the duplicate clusters detected by the Cd4py tool, finds the duplicates and deletes them."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-d', required=True, type=str,
                            help="Path to duplicate list")
    arg_parser.add_argument('-o', required=True,
                            type=str, help="Output file")
    arg_parser.add_argument('-p', required=False, default="empty",
                            type=str, help="Dataset folder from which files should be kept preferentially")
    args = arg_parser.parse_args()

    delete_duplicates(args.d, args.o, args.p)


if __name__ == "__main__":
    main()
