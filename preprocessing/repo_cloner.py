import argparse
import os
import re
import subprocess
import time
from concurrent.futures import ThreadPoolExecutor
from urllib.error import HTTPError
from urllib.request import urlopen


def is_repo_accessible(url: str, wait: int = 10) -> bool:
    while True:
        try:
            urlopen(url)
            return True
        except HTTPError as e:
            if e.code == 429:
                time.sleep(wait)
                continue
            return False


def download_git_repository(url, output_dir, full_fetch=True):
    command = ["git", "clone"]
    if not full_fetch:
        command += ["--depth", "1"]
    command += [url, output_dir]
    subprocess.run(command)


def download_project(url, output_dir, full_fetch):
    try:
        name = re.findall(r"https://github.com/(.*).git", url)[0]
        url = url.replace("\n", "")
        output_dir = os.path.join(output_dir, name)
        if os.path.isdir(output_dir):
            print(f"{name} already exists")
            return False
        if is_repo_accessible(url):
            download_git_repository(url, output_dir, full_fetch=full_fetch)
            return True
        else:
            print(f"The project {url} doesn't exist or it's private.")
            return False
    except Exception as e:
        print(f"could not download {name}: {e}")


def download_repos(projects, output_dir, full_fetch):
    with ThreadPoolExecutor(max_workers=5) as executor:
        executor.map(
            lambda p: download_project(
                p, output_dir, full_fetch), projects
        )


def load_repo_list(input_file):
    gh_repos = []
    with open(input_file, "r") as f:
        lines = f.readlines()

    for line in lines:
        gh_repos.append(line.split(" ")[0])
    return gh_repos


def main():
    """Clones the repositories from the dataset specification file."""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-r', required=True, type=str,
                            help="Path to repository list")
    arg_parser.add_argument('-o', required=True,
                            type=str, help="Output directory")
    args = arg_parser.parse_args()
    repo_list = load_repo_list(args.r)

    download_repos(repo_list, args.o, full_fetch=True)


if __name__ == "__main__":
    main()
