import logging
import multiprocessing
import os
from argparse import ArgumentParser
from time import time

import numpy as np
import pandas as pd
from gensim.models import Word2Vec
from tqdm import tqdm

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

logger_sh = logging.StreamHandler()
logger_sh.setLevel(logging.DEBUG)
logger_sh.setFormatter(
    logging.Formatter(
        fmt="[%(asctime)s][%(name)s][%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
)
logger.addHandler(logger_sh)

MIN_DATA_POINTS = 3
AVAILABLE_TYPES_NUMBER = 1024
KNN_TREE_SIZE = 20
MAX_PARAM_TYPE_DEPTH = 2
TOKEN_SEQ_LEN = (7, 3)


logger.name = __name__
tqdm.pandas()

W2V_VEC_LENGTH = 100
total_key_errors = 0
total_key_success = 0
total_keys = 0


def mk_dir_not_exist(dirs):
    """
    dirs - a list of directories to create if these directories are not found
    :param dirs:
    :return exit_code: 0:success -1:failed
    """
    try:
        for dir_ in dirs:
            if not os.path.exists(dir_):
                os.makedirs(dir_)
        return 0
    except Exception as err:
        print("Creating directories error: {0}".format(err))
        exit(-1)


class TokenIterator:
    def __init__(self, ds1, ds2,  oov_mode) -> None:
        self.ds1 = ds1
        self.ds2 = ds2
        self.oov_mode = oov_mode

    def __iter__(self):
        dataframes = []
        if self.oov_mode == "realistic":
            dataframes = [self.ds1[0]]
        elif self.oov_mode == "optimistic":
            dataframes = [self.ds1[0], self.ds2[0]]
        elif self.oov_mode == "best":
            dataframes = [
                self.ds1[0],
                self.ds2[0],
                self.ds1[1],
                self.ds2[1],
                self.ds1[2],
                self.ds2[2],
            ]
        else:
            raise NotImplementedError

        for df in dataframes:
            param_df = df[0]
            return_df = df[1]
            var_df = df[2]
            for return_expr_sentences in return_df["return_expr_str"][
                return_df["return_expr_str"] != ""
            ]:
                yield return_expr_sentences.split()

            for code_occur_sentences in param_df["arg_occur"][
                param_df["arg_occur"] != ""
            ]:
                yield code_occur_sentences.split()

            for func_name_sentences in param_df["func_name"][
                ~param_df["func_name"].isnull()
            ]:
                yield func_name_sentences.split()

            for arg_names_sentences in return_df["arg_names_str"][
                return_df["arg_names_str"] != ""
            ]:
                yield arg_names_sentences.split()

            for var_names_sentences in var_df["var_name"][var_df["var_name"].notnull()]:
                yield var_names_sentences.split()

            for var_occur_sentences in var_df["var_occur"][var_df["var_occur"] != ""]:
                yield var_occur_sentences.split()


class W2VEmbedding:
    """
    Word2Vec embeddings for code tokens and identifiers
    """

    def __init__(self, ds1, ds2, w2v_model_tk_path, oov_mode) -> None:
        self.ds1 = ds1
        self.ds2 = ds2
        self.oov_mode = oov_mode
        self.w2v_model_tk_path = w2v_model_tk_path

    def train_model(self, corpus_iterator: TokenIterator, model_path_name: str) -> None:
        """
        Train a Word2Vec model and save the output to a file.
        :param corpus_iterator: class that can provide an iterator that goes through the corpus
        :param model_path_name: path name of the output file
        """

        w2v_model = Word2Vec(
            min_count=5,
            window=5,
            vector_size=W2V_VEC_LENGTH,
            workers=multiprocessing.cpu_count(),
        )

        t = time()
        w2v_model.build_vocab(corpus_iterable=corpus_iterator)
        logger.info("Built W2V vocab in {} mins".format(
            round((time() - t) / 60, 2)))
        logger.info(f"W2V model's vocab size: {len(w2v_model.wv):,}")

        t = time()
        w2v_model.train(
            corpus_iterable=corpus_iterator,
            total_examples=w2v_model.corpus_count,
            epochs=20,
            report_delay=1,
        )

        logger.info("Built W2V model in {} mins".format(
            round((time() - t) / 60, 2)))
        w2v_model.save(model_path_name)

    def train_token_model(self):
        """
        Trains a W2V model for tokens.
        """
        self.train_model(
            TokenIterator(self.ds1, self.ds2,  self.oov_mode),
            self.w2v_model_tk_path,
        )


def vectorize_sequence(
    sequence: str, feature_length: int, w2v_model: Word2Vec
) -> np.ndarray:
    """
    Vectorize a sequence to a multi-dimensial NumPy array
    """

    vector = np.zeros((feature_length, W2V_VEC_LENGTH))
    global total_key_errors
    global total_key_success
    global total_keys

    for i, word in enumerate(sequence.split()):
        if i >= feature_length:
            break
        try:
            total_keys += 1
            vector[i] = w2v_model.wv[word]
            total_key_success += 1
        except KeyError:
            total_key_errors += 1

    return vector


class IdentifierSequence:
    """
    Vector representation of identifiers
    """

    def __init__(self, identifiers_embd, arg_name, args_name, func_name, var_name):

        self.identifiers_embd = identifiers_embd
        self.arg_name = arg_name
        self.args_name = args_name
        self.func_name = func_name
        self.var_name = var_name

    def seq_length_param(self):

        return {"arg_name": 10, "sep": 1, "func_name": 10, "args_name": 10}

    def seq_length_return(self):

        return {"func_name": 10, "sep": 1, "args_name": 10, "padding": 10}

    def seq_length_var(self):
        return {"var_name": 10, "padding": 21}

    def __gen_datapoint(self, seq_length):
        datapoint = np.zeros((sum(seq_length.values()), W2V_VEC_LENGTH))
        separator = np.ones(W2V_VEC_LENGTH)

        p = 0
        for seq, length in seq_length.items():
            if seq == "sep":
                datapoint[p] = separator
                p += 1
            elif seq == "padding":
                for i in range(0, length):
                    datapoint[p] = np.zeros(W2V_VEC_LENGTH)
                    p += 1
            else:
                try:
                    for w in vectorize_sequence(
                        self.__dict__[seq], length, self.identifiers_embd
                    ):
                        datapoint[p] = w
                        p += 1
                except AttributeError:
                    pass

        return datapoint

    def generate_datapoint(self):
        if (
            self.arg_name is not None
            and self.args_name is not None
            and self.func_name is None
        ):
            return self.__gen_datapoint(self.seq_length_param())
        elif self.args_name is not None and self.func_name is not None:
            return self.__gen_datapoint(self.seq_length_return())
        elif self.var_name is not None:
            return self.__gen_datapoint(self.seq_length_var())


class TokenSequence:
    """
    Vector representation of code tokens
    """

    def __init__(
        self,
        token_model,
        len_tk_seq,
        num_tokens_seq,
        args_usage,
        return_expr,
        var_usage,
    ):
        self.token_model = token_model
        self.len_tk_seq = len_tk_seq
        self.num_tokens_seq = num_tokens_seq
        self.args_usage = args_usage
        self.return_expr = return_expr
        self.var_usage = var_usage

    def param_datapoint(self):
        datapoint = np.zeros(
            (self.num_tokens_seq * self.len_tk_seq, W2V_VEC_LENGTH))
        for i, w in enumerate(
            vectorize_sequence(
                self.args_usage, self.num_tokens_seq * self.len_tk_seq, self.token_model
            )
        ):
            datapoint[i] = w
        return datapoint

    def return_datapoint(self):
        datapoint = np.zeros(
            (self.num_tokens_seq * self.len_tk_seq, W2V_VEC_LENGTH))

        if isinstance(self.return_expr, str):
            p = 0
            for w in vectorize_sequence(
                self.return_expr, self.len_tk_seq, self.token_model
            ):
                datapoint[p] = w
                p += 1
        return datapoint

    def var_datapoint(self):
        datapoint = np.zeros(
            (self.num_tokens_seq * self.len_tk_seq, W2V_VEC_LENGTH))
        for i, w in enumerate(
            vectorize_sequence(
                self.var_usage, self.num_tokens_seq * self.len_tk_seq, self.token_model
            )
        ):
            datapoint[i] = w
        return datapoint

    def generate_datapoint(self):
        if self.args_usage is not None:
            return self.param_datapoint()
        elif self.return_expr is not None:
            return self.return_datapoint()
        elif self.var_usage is not None:
            return self.var_datapoint()


def process_datapoints(
    df, output_path, embedding_type, type, trans_func, cached_file: bool = True
):

    if (
        not os.path.exists(
            os.path.join(output_path, embedding_type +
                         type + "_datapoints_x.npy")
        )
        or not cached_file
    ):
        datapoints = df.apply(trans_func, axis=1)

        datapoints_X = np.stack(
            datapoints.progress_apply(lambda x: x.generate_datapoint()), axis=0
        )
        np.save(
            os.path.join(output_path, embedding_type + type + "_datapoints_x"),
            datapoints_X,
        )

        return datapoints_X
    else:
        logger.warn(f"file {embedding_type + type + '_datapoints_x'} exists!")
        return


def type_vector(size, index):
    v = np.zeros(size)
    v[index] = 1
    return v


def gen_aval_types_datapoints(
    df_params,
    df_ret,
    df_var,
    set_type,
    type_hints,
    output_path,
    cached_file: bool = False,
):
    """
    It generates data points for available types.
    """
    for th in type_hints:
        aval_types_params = np.stack(
            df_params.progress_apply(
                lambda row: type_vector(
                    AVAILABLE_TYPES_NUMBER, eval(f"row.param_aval_{th}_enc")
                ),
                axis=1,
            ),
            axis=0,
        )
        aval_types_ret = np.stack(
            df_ret.progress_apply(
                lambda row: type_vector(
                    AVAILABLE_TYPES_NUMBER, eval(f"row.ret_aval_{th}_enc")
                ),
                axis=1,
            ),
            axis=0,
        )
        aval_types_var = np.stack(
            df_var.progress_apply(
                lambda row: type_vector(
                    AVAILABLE_TYPES_NUMBER, eval(f"row.var_aval_{th}_enc")
                ),
                axis=1,
            ),
            axis=0,
        )

        np.save(
            os.path.join(output_path, f"params_{set_type}_aval_types_dp_{th}"),
            aval_types_params,
        )
        np.save(
            os.path.join(output_path, f"ret_{set_type}_aval_types_dp_{th}"),
            aval_types_ret,
        )
        np.save(
            os.path.join(output_path, f"var_{set_type}_aval_types_dp_{th}"),
            aval_types_var,
        )


def gen_labels_vector(
    params_df: pd.DataFrame,
    returns_df: pd.DataFrame,
    var_df: pd.DataFrame,
    set_type: str,
    output_path: str,
):
    """
    It generates a flattened labels vector
    """
    np.save(
        os.path.join(output_path, f"params_{set_type}_dps_y_all"),
        params_df["arg_type_enc_all"].values,
    )
    np.save(
        os.path.join(output_path, f"ret_{set_type}_dps_y_all"),
        returns_df["return_type_enc_all"].values,
    )
    np.save(
        os.path.join(output_path, f"var_{set_type}_dps_y_all"),
        var_df["var_type_enc_all"].values,
    )

    return (
        params_df["arg_type_enc_all"].values,
        returns_df["return_type_enc_all"].values,
        var_df["var_type_enc_all"].values,
    )


def vectorize_args_ret(path_ds: list, type_hints: list, oov_mode: str):
    """
    Creates vector representation of functions' arguments and return values
    """

    def load_data(path):
        train_param_df = pd.read_csv(
            os.path.join(path, "_ml_param_train.csv"), na_filter=False
        )
        train_return_df = pd.read_csv(
            os.path.join(path, "_ml_ret_train.csv"), na_filter=False
        )
        train_var_df = pd.read_csv(
            os.path.join(path, "_ml_var_train.csv"), na_filter=False
        )
        logger.info("Loaded the training data")

        valid_param_df = pd.read_csv(
            os.path.join(path, "_ml_param_valid.csv"), na_filter=False
        )
        valid_return_df = pd.read_csv(
            os.path.join(path, "_ml_ret_valid.csv"), na_filter=False
        )
        valid_var_df = pd.read_csv(
            os.path.join(path, "_ml_var_valid.csv"), na_filter=False
        )
        logger.info("Loaded the validation data")

        test_param_df = pd.read_csv(
            os.path.join(path, "_ml_param_test.csv"), na_filter=False
        )
        test_return_df = pd.read_csv(
            os.path.join(path, "_ml_ret_test.csv"), na_filter=False
        )
        test_var_df = pd.read_csv(
            os.path.join(path, "_ml_var_test.csv"), na_filter=False
        )
        logger.info("Loaded the test data")
        return (
            [train_param_df, train_return_df, train_var_df],
            [valid_param_df, valid_return_df, valid_var_df],
            [test_param_df, test_return_df, test_var_df],
        )

    mt4p_train, mt4p_val, mt4p_test = load_data(path_ds[0])
    np_train, np_val, np_test = load_data(path_ds[1])

    datasets = [
        [
            [mt4p_train, mt4p_val, mt4p_test],
            type_hints,
            path_ds[0],
        ],
        [[np_train, np_val, np_test], type_hints, path_ds[1]],
    ]

    if not os.path.exists(os.path.join(path_ds[0], "w2v_token_model.bin")):
        embedder = W2VEmbedding(
            [mt4p_train, mt4p_val, mt4p_test],
            [np_train, np_val, np_test],
            os.path.join(path_ds[0], "w2v_token_model.bin"),
            oov_mode=oov_mode,
        )
        embedder.train_token_model()
    else:
        logger.warn("Loading an existing pre-trained W2V model!")

    w2v_token_model = Word2Vec.load(
        os.path.join(path_ds[0], "w2v_token_model.bin"))

    vts_seq_len = (15, 5)
    # Vectorize functions' arguments

    def id_trans_func_param(row): return IdentifierSequence(
        w2v_token_model, row.arg_name, row.other_args, row.func_name, None
    )

    def token_trans_func_param(row): return TokenSequence(
        w2v_token_model, TOKEN_SEQ_LEN[0], TOKEN_SEQ_LEN[1], row.arg_occur, None, None
    )

    def process_identifiers(dataset):
        # Create dirs for vectors
        data = dataset[0]
        type_hints = dataset[1]
        output_path = dataset[2]
        mk_dir_not_exist(
            [
                os.path.join(output_path, "vectors"),
                os.path.join(output_path, "vectors", "train"),
                os.path.join(output_path, "vectors", "valid"),
                os.path.join(output_path, "vectors", "test"),
            ]
        )
        train_param_df = data[0][0]
        train_return_df = data[0][1]
        train_var_df = data[0][2]
        valid_param_df = data[1][0]
        valid_return_df = data[1][1]
        valid_var_df = data[1][2]
        test_param_df = data[2][0]
        test_return_df = data[2][1]
        test_var_df = data[2][2]

        # Identifiers
        logger.info("[arg][identifiers] Generating vectors")
        process_datapoints(
            train_param_df,
            os.path.join(output_path, "vectors", "train"),
            "identifiers_",
            "param_train",
            id_trans_func_param,
        )
        process_datapoints(
            valid_param_df,
            os.path.join(output_path, "vectors", "valid"),
            "identifiers_",
            "param_valid",
            id_trans_func_param,
        )
        process_datapoints(
            test_param_df,
            os.path.join(output_path, "vectors", "test"),
            "identifiers_",
            "param_test",
            id_trans_func_param,
        )

        # Tokens
        logger.info("[arg][code tokens] Generating vectors")
        process_datapoints(
            train_param_df,
            os.path.join(output_path, "vectors", "train"),
            "tokens_",
            "param_train",
            token_trans_func_param,
        )
        process_datapoints(
            valid_param_df,
            os.path.join(output_path, "vectors", "valid"),
            "tokens_",
            "param_valid",
            token_trans_func_param,
        )
        process_datapoints(
            test_param_df,
            os.path.join(output_path, "vectors", "test"),
            "tokens_",
            "param_test",
            token_trans_func_param,
        )

        # Vectorize functions' return types
        def id_trans_func_ret(row): return IdentifierSequence(
            w2v_token_model, None, row.arg_names_str, row.name, None
        )

        def token_trans_func_ret(row): return TokenSequence(
            w2v_token_model,
            TOKEN_SEQ_LEN[0],
            TOKEN_SEQ_LEN[1],
            None,
            row.return_expr_str,
            None,
        )

        # Identifiers
        logger.info("[ret][identifiers] Generating vectors")
        process_datapoints(
            train_return_df,
            os.path.join(output_path, "vectors", "train"),
            "identifiers_",
            "ret_train",
            id_trans_func_ret,
        )
        process_datapoints(
            valid_return_df,
            os.path.join(output_path, "vectors", "valid"),
            "identifiers_",
            "ret_valid",
            id_trans_func_ret,
        )
        process_datapoints(
            test_return_df,
            os.path.join(output_path, "vectors", "test"),
            "identifiers_",
            "ret_test",
            id_trans_func_ret,
        )

        # Tokens
        logger.info("[ret][code tokens] Generating vectors")
        process_datapoints(
            train_return_df,
            os.path.join(output_path, "vectors", "train"),
            "tokens_",
            "ret_train",
            token_trans_func_ret,
        )
        process_datapoints(
            valid_return_df,
            os.path.join(output_path, "vectors", "valid"),
            "tokens_",
            "ret_valid",
            token_trans_func_ret,
        )
        process_datapoints(
            test_return_df,
            os.path.join(output_path, "vectors", "test"),
            "tokens_",
            "ret_test",
            token_trans_func_ret,
        )

        # Vectorize variables types
        def id_trans_func_var(row): return IdentifierSequence(
            w2v_token_model, None, None, None, row.var_name
        )

        def token_trans_func_var(row): return TokenSequence(
            w2v_token_model,
            TOKEN_SEQ_LEN[0],
            TOKEN_SEQ_LEN[1],
            None,
            None,
            row.var_occur,
        )

        # Identifiers
        logger.info("[var][identifiers] Generating vectors")
        process_datapoints(
            train_var_df,
            os.path.join(output_path, "vectors", "train"),
            "identifiers_",
            "var_train",
            id_trans_func_var,
        )
        process_datapoints(
            valid_var_df,
            os.path.join(output_path, "vectors", "valid"),
            "identifiers_",
            "var_valid",
            id_trans_func_var,
        )
        process_datapoints(
            test_var_df,
            os.path.join(output_path, "vectors", "test"),
            "identifiers_",
            "var_test",
            id_trans_func_var,
        )

        # Tokens
        logger.info("[var][code tokens] Generating vectors")
        process_datapoints(
            train_var_df,
            os.path.join(output_path, "vectors", "train"),
            "tokens_",
            "var_train",
            token_trans_func_var,
        )
        process_datapoints(
            valid_var_df,
            os.path.join(output_path, "vectors", "valid"),
            "tokens_",
            "var_valid",
            token_trans_func_var,
        )
        process_datapoints(
            test_var_df,
            os.path.join(output_path, "vectors", "test"),
            "tokens_",
            "var_test",
            token_trans_func_var,
        )

        logger.info("[visible type hints] Generating vectors")
        gen_aval_types_datapoints(
            train_param_df,
            train_return_df,
            train_var_df,
            "train",
            type_hints,
            os.path.join(output_path, "vectors", "train"),
        )
        gen_aval_types_datapoints(
            valid_param_df,
            valid_return_df,
            valid_var_df,
            "valid",
            type_hints,
            os.path.join(output_path, "vectors", "valid"),
        )
        gen_aval_types_datapoints(
            test_param_df,
            test_return_df,
            test_var_df,
            "test",
            type_hints,
            os.path.join(output_path, "vectors", "test"),
        )

        # a flattened vector for labels
        logger.info("[true labels] Generating vectors")
        gen_labels_vector(
            train_param_df,
            train_return_df,
            train_var_df,
            "train",
            os.path.join(output_path, "vectors", "train"),
        )
        gen_labels_vector(
            valid_param_df,
            valid_return_df,
            valid_var_df,
            "valid",
            os.path.join(output_path, "vectors", "valid"),
        )
        gen_labels_vector(
            test_param_df,
            test_return_df,
            test_var_df,
            "test",
            os.path.join(output_path, "vectors", "test"),
        )

    for dataset in datasets:
        global total_key_errors
        global total_key_success
        global total_keys
        total_key_errors = 0
        total_key_success = 0
        total_keys = 0
        process_identifiers(dataset)
        print(
            f"Total keys {total_keys} - success {total_key_success} errors {total_key_errors} in project {dataset[2]}")


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Preprocess JSON output")
    arg_parser.add_argument("first_ds", type=str,
                            help="First dataset directory")
    arg_parser.add_argument("first_th_name", type=str,
                            help="Name of the type hints for first dataset")
    arg_parser.add_argument("second_ds", type=str,
                            help="Second dataset directory")
    arg_parser.add_argument("second_th_name", type=str,
                            help="Name of the type hints for second dataset")
    arg_parser.add_argument("train_mode", type=str,
                            help="Word2Vec model train mode")

    args = arg_parser.parse_args()
    ds_path = [args.first_ds, args.second_ds]
    type_hints = [f"{args.first_th_name}_VTHs", f"{args.second_th_name}_VTHs"]
    vectorize_args_ret(ds_path, type_hints, args.train_mode)
