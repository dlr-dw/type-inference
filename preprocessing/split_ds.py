import os
import sys
from argparse import ArgumentParser
from pathlib import Path

import pandas as pd
from sklearn.model_selection import train_test_split


def list_files(directory: str) -> list:
    """
    List all .py files recursively in the given directory.
    :param directory: directory to search in
    :return: list of python code files
    """
    paths = []

    for root, dirs, files in os.walk(directory):
        paths += [os.path.join(root, fname)
                  for fname in files if fname.endswith(".py")]

    return paths


def get_repo_list(ds_path):
    subfolders = [f.path for f in os.scandir(ds_path) if f.is_dir()]
    all_projects = []
    for subfolder in subfolders:
        projects = [f.path for f in os.scandir(subfolder) if f.is_dir()]
        for project in projects:
            split = project.split("/")
            all_projects.append(f"{split[-2]}/{split[-1]}")
    return all_projects


def main():
    arg_parser = ArgumentParser(
        description="Dataset split (train, test, validation) for dataset"
    )
    arg_parser.add_argument("dataset", type=str, help="Dataset directory")
    arg_parser.add_argument(
        "--od", type=str, help="CSV split output path", default="dataset_split.csv"
    )
    arg_parser.add_argument(
        "--test", type=float, help="Test split ratio: [0.0, 1.0)", default=0.2
    )
    arg_parser.add_argument(
        "--valid", type=float, help="Validation split ratio: [0.0, 1.0)", default=0.1
    )

    args = arg_parser.parse_args()
    DATASET_DIR = args.dataset
    OUTPUT_PATH = args.od
    TEST_SIZE = args.test
    VALID_SIZE = args.valid

    repo_list = get_repo_list(DATASET_DIR)
    print(f"Found {len(repo_list)} repositories in {DATASET_DIR}")

    train_repos, test_repos = train_test_split(
        repo_list, test_size=TEST_SIZE
    )
    train_repos, valid_repos = train_test_split(
        train_repos,
        test_size=VALID_SIZE,)

    data_files = list_files(DATASET_DIR)
    print(f"Found {len(data_files)} python files")
    df = pd.DataFrame(data_files, columns=["file"])

    df["type"] = ""
    for index, row in df.iterrows():
        if any(repo in row['file'] for repo in train_repos):
            df.iloc[index]["type"] = "train"
            df.iloc[index]["file"] = Path(df.iloc[index]["file"]).relative_to(
                *Path(df.iloc[index]["file"]).parts[:2])
        elif any(repo in row['file'] for repo in valid_repos):
            df.iloc[index]["type"] = "valid"
            df.iloc[index]["file"] = Path(df.iloc[index]["file"]).relative_to(
                *Path(df.iloc[index]["file"]).parts[:2])
        elif any(repo in row['file'] for repo in test_repos):
            df.iloc[index]["type"] = "test"
            df.iloc[index]["file"] = Path(df.iloc[index]["file"]).relative_to(
                *Path(df.iloc[index]["file"]).parts[:2])

    print(
        f"Number of files in train set: {len(df.loc[df['type'] == 'train'])}")
    print(
        f"Number of files in validation set: {len(df.loc[df['type'] == 'valid'])}")
    print(f"Number of files in test set: {len(df.loc[df['type'] == 'test'])}")

    # https://stackoverflow.com/questions/36303919/python-3-0-open-default-encoding
    # https://stackoverflow.com/questions/1052225/convert-python-filenames-to-unicode
    df = df[["type", "file"]]
    print("Writing dataframe to:", OUTPUT_PATH)
    df.to_csv(
        OUTPUT_PATH, header=False, index=False, encoding=sys.getfilesystemencoding()
    )


if __name__ == "__main__":
    main()
